# BLT (Version 1.0)

BLT (short for "Benchmarking and Linearizability checking Tool") provides an easy interface to check linearizability and
benchmark concurrent data structures. The linearizability checking component
utilizes [Lincheck](https://github.com/Kotlin/kotlinx-lincheck), a tool written by researchers from JetBrains and IST
Austria to check datastructures written in languages running on the Java Virtual Machine for linearizability. Since
benchmarking in the JVM can be quite error-prone due to performance inconsistencies arising from e.g. JIT-Compilation
with different optimization levels, the framework uses the
[Java Microbenchmark Harness](https://github.com/openjdk/jmh) to execute synthesized benchmarks for consistent
performance measurements.

## Example Usage

To use data structures with this framework, a so-called wrapper class encapsulating a data structure has to be written
to specify the interface and methods the data structure provides. See the following example encapsulating
the `ConcurrentLinkedQueue` data structure from the Java Standard library. Note that operations are marked with
`@Operation` and the class is annotated with `@BenchmarkWrapper`.

```java
@BenchmarkWrapper
public class ConcurrentLinkedQueueWrapper extends VerifierState {
    private final ConcurrentLinkedQueue<Integer> q = new ConcurrentLinkedQueue<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q;
    }
}
```

### Linearizability checking

The wrapper class allows to check the data structure for linearizability using two different strategies. One strategy is
the `StressTest`-Strategy, which stress tests the data structure, collects the thread history and then checks the
results for linearizability using a labelled transition system. Another provided way of checking data structures for
linearizability is the `Model Checking`-Strategy that verifies linearizability under a bound number of thread context
switches. By default, both strategies will be applied since this guarantees better results, but since the model checking
strategy sometimes still encounters problems, a linearizability checking strategy can be selected.

See the following example for a linearizability check of a data structure with the corresponding output:

```java
LinCheckRunner.run(ConcurrentQueueWrapper.class);
```

```
Executing linearizability test in stress testing mode.
Linearizability test in stress testing mode exited successfully.
Executing linearizability test in model checking mode.
Linearizability test in model checking mode exited successfully.
```

### Benchmarking

In a similar way to linearizability checking, the framework allows to benchmark the throughput of data structure
operations with many possible customization options. Benchmarks are synthesized according to the data structure
specification and executed using JMH for a controlled execution environment and consistent results.

To execute a (randomly generated) benchmark, run:

```java
BenchmarkRunner.run(ConcurrentLinkedQueueWrapper.class);
```

Many parameters of the benchmark can be customized, like the composition of operations to be executed, number of threads
to execute the benchmark with or JMH-internal parameters such as the number of measurement iterations. This
functionality is provided by the `BenchmarkOptions` class, which allows to pass the intended configuration to the
benchmark runner.

The following configuration runs a randomly generated benchmark for the wrapper class with 1 and 2 threads and 3
measurement iterations, respectively.

```
BenchmarkOptions opt = new BenchmarkOptionsBuilder().threads(1,2).measurementIterations(3).build();
BenchmarkRunner.run(ConcurrentLinkedQueueWrapper.class, opt);
```

The following results are reported to the command line and saved to a report file after benchmark execution:

```
========== BENCHMARK RESULTS ==========

Throughput Results:
threads, mean score (ops/us), conf95 (+/-)
1, 130.881655, 0.877943
2, 248.227501, 5.829465
```

In addition to this file, the concrete benchmark scenario and JMH profiler data like thread states are written to the
report directory.

#### Benchmark Scenarios

When executing a benchmark like above, a random scenario will be generated for the data structure. The concrete scenario
is always reported after benchmark execution to `scenario.txt` in the result folder and may look like the following:

```
init: {  }

thread 1: { add(1830475215), remove(1807534469), contains(965128807) }
thread 2: { add(794787308), remove(1592182240), contains(-70596866) }
```

To allow for a more flexible benchmark configuration, user-defined benchmark scenarios may be provided with which, for
example, producer-consumer systems or similar may be defined. See the following example for a benchmark with four
threads:

```java
Class<?> wrapperClass = DraconicWrapper.class;
Method addMethod = wrapperClass.getMethod("add",Integer.class);
Method removeMethod = wrapperClass.getMethod("remove",Integer.class);
Method contains = wrapperClass.getMethod("contains",Integer.class);

ThreadScenario add = new ThreadScenarioBuilder(wrapperClass).addMethodCall(addMethod).build();
ThreadScenario rem = new ThreadScenarioBuilder(wrapperClass).addMethodCall(removeMethod).build();
ThreadScenario con = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains).build();

ThreadScenario initSce = new ThreadScenarioBuilder(wrapperClass).addMethodCall(addMethod,1337).build();

BenchmarkScenario bs = new BenchmarkScenarioBuilder().addThreadScenarios(add,rem,con,con).scenarioSize(4).build();

BenchmarkOptions opt = new BenchmarkOptionsBuilder()
    .benchmarkScenario(bs)
    .threads(1,2)
    .initScenario(initSce)
    .build();

BenchmarkRunner.run(wrapperClass,opt);
```

This configuration results in the following scenario:
```
init: { add(1337) }

thread 1: { add(1348438782), add(1892008641), add(1179063889), add(2087973107) }
thread 2: { remove(-220080741), remove(2043158209), remove(1662522909), remove(1359962274) }
thread 3: { contains(933161371), contains(-1085685442), contains(1813337397), contains(1854938945) }
thread 4: { contains(2028786735), contains(1426093693), contains(2056908112), contains(-1492848320) }
```

#### Performance Counters

For the purpose of reporting internals of (mainly wait-free) data structures, a data structure may expose
"Performance Counters" whose values will be recorded after benchmark runs and be included in the benchmark results.

These performance counters can be specified in the wrapper class as follows:

```java
@PerformanceCounter
public long trav(){
    return q.getTrav();
}

@PerformanceCounter
public long cons(){
    return q.getCons();
}
```

The benchmark results and command line output will now contain additional output reporting the average values and
confidence interval of the performance counter measurements:

```
threads, name, average, conf95 (+/-)
1, trav, 392687114, 20451095
1, cons, 150324323, 9342333
2, trav, 551909114, 42288288
2, cons, 261835682, 16318170
```

