package at.ac.tuwien.blt.processed;

import at.ac.tuwien.blt.benchmark.builder.BuilderConfiguration;
import at.ac.tuwien.blt.benchmark.builder.jmh.ThreadState;
import at.ac.tuwien.blt.benchmark.builder.jmh.CounterMap;
import at.ac.tuwien.blt.benchmark.builder.jmh.BenchmarkData;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.*;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;

@State(Scope.Benchmark)
public class %s {

    public %s dataStructure;

    protected Object[][] initArgs;

    private CounterMap counterMap;

    @Setup(Level.Trial)
    public void prepareCounters(BenchmarkParams params) {
        String filename = String.format(BuilderConfiguration.PERFORMANCE_COUNTER_FORMAT_STRING, params.getThreads());
        try {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            counterMap = (CounterMap) in.readObject();
            in.close();
        } catch (FileNotFoundException fnf) {
            counterMap = new CounterMap();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Setup(Level.Iteration)
    public void prepare() {
        String filename = String.format(BuilderConfiguration.THREAD_DATA_FORMAT_STRING, 0);
        BenchmarkData initData;
        int initialSize;
        dataStructure = new %s();

        try {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            initData = (BenchmarkData) in.readObject();
            in.close();
            fileIn.close();
            initArgs = initData.parameters;
            initialSize = initData.size;

            if (initArgs.length < initialSize) {
                throw new RuntimeException("init parameters array is too small");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error reading thread benchmark data from file \"" + filename + "\"");
        }

        %s
    }


    @TearDown(Level.Iteration)
    public void putCounterValues(BenchmarkParams params) {
        long counterValue; %s

    }

    @TearDown(Level.Trial)
    public void saveToFile(BenchmarkParams params) {
        String path = String.format(BuilderConfiguration.PERFORMANCE_COUNTER_FORMAT_STRING, params.getThreads());
        // write to outputFile
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(counterMap);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to write performance counter data to file \"" + path + "\"");
        }
    }
    %s
}
