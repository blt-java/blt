package at.ac.tuwien.blt.lincheck;

public enum LinCheckStrategy {
    STRESS_TEST, MODEL_CHECKING, MIXED
}
