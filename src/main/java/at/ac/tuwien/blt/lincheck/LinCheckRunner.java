package at.ac.tuwien.blt.lincheck;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.LinChecker;
import org.jetbrains.kotlinx.lincheck.LincheckAssertionError;
import org.jetbrains.kotlinx.lincheck.Options;
import org.jetbrains.kotlinx.lincheck.strategy.managed.modelchecking.ModelCheckingOptions;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressOptions;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class LinCheckRunner {
    static final String LINCHECK_FILE_NAME = "lincheck_result.txt";

    /**
     * Test a LinCheck test class with stress test strategy.
     *
     * @param testClass the class to be tested
     * @param resultPath the (classpath-relative) path to save the output file to
     */
    static public void run(@NotNull Class<?> testClass, String resultPath) {
        run_stress_test(testClass, resultPath);
    }

    /**
     * Test a LinCheck test class with model checking strategy.
     *
     * @param testClass the class to be tested
     */
    static public void run_model_checking(@NotNull Class<?> testClass) {
        List<Options<?, ?>> options = Collections.singletonList(new ModelCheckingOptions());
        run(testClass, options, LinCheckStrategy.MODEL_CHECKING, null);
    }

    /**
     * Test a LinCheck test class with model checking strategy.
     *
     * @param testClass the class to be tested
     * @param resultPath the (classpath-relative) path to save the output file to
     */
    static public void run_model_checking(@NotNull Class<?> testClass, String resultPath) {
        List<Options<?, ?>> options = Collections.singletonList(new ModelCheckingOptions());
        run(testClass, options, LinCheckStrategy.MODEL_CHECKING, resultPath);
    }

    /**
     * Test a LinCheck test class with stress test strategy.
     *
     * @param testClass the class to be tested
     * @param resultPath the (classpath-relative) path to save the output file to
     */
    static public void run_stress_test(@NotNull Class<?> testClass, String resultPath) {
        List<Options<?, ?>> options = Collections.singletonList(new StressOptions());
        run(testClass, options, LinCheckStrategy.STRESS_TEST, resultPath);
    }

    /**
     * Test a LinCheck test class with stress test strategy.
     *
     * @param testClass the class to be tested
     */
    static public void run_stress_test(@NotNull Class<?> testClass) {
        List<Options<?, ?>> options = Collections.singletonList(new StressOptions());
        run(testClass, options, LinCheckStrategy.STRESS_TEST);
    }

    static private void log_result(boolean linearizablityResult, String errorMessage, Options<?,?> options, String path)
    {
        if (path == null) {
            return;
        }

        String message = "lincheck (" + optionRepString(options) + "): ";

        if (linearizablityResult) {
            // data structure is linearizable
            message += "successful\n";
        } else {
            // data structure is not linearizable
            message += "failed\n";
            message += "Error: " + errorMessage + "\n";
        }

        message += "\n";

        try {
            String p;
            if (path.endsWith("/")) {
                p = path + LINCHECK_FILE_NAME;
            } else {
                p = path + "" + LINCHECK_FILE_NAME;
            }
            FileWriter fw = new FileWriter(p, true);
            fw.write(message);
            fw.close();
        } catch (IOException e) {
            System.err.println("Failed to write linearizability result to output file due to the following error:");
            System.err.println("\t"+e+"\n");
        }
    }

    /**
     * Test a LinCheck test class with stress test strategy.
     *
     * @param testClass the class to be tested
     */
    static public void run(@NotNull Class<?> testClass) {
        run(testClass, (String) null);
    }

    /**
     * Run a LinCheck test with a specified strategy
     *
     * @param testClass wrapper class with operation annotated methods
     * @param options   array of options that the class should be tested with
     * @param strategy  the strategy to be tested with (stress test, model checking, mixed)
     */
    static public void run(@NotNull Class<?> testClass, List<Options<?, ?>> options,
                                    LinCheckStrategy strategy) {
        run(testClass, options, strategy, null);
    }

    /**
     * Run a LinCheck test with a specified strategy
     *
     * @param testClass wrapper class with operation annotated methods
     * @param options   array of options that the class should be tested with
     * @param strategy  the strategy to be tested with (stress test, model checking, mixed)
     * @param resultPath the (classpath-relative) path to save the output file to
     */
    public static void run(@NotNull Class<?> testClass, List<Options<?, ?>> options,
                           LinCheckStrategy strategy, String resultPath) {
        switch (strategy) {
            case MIXED:
                for (var opt : options) {
                    if (opt.getClass() == StressOptions.class
                            || opt.getClass() == ModelCheckingOptions.class) {
                        run_internal(testClass, opt, resultPath);
                    } else {
                        printInvalidOption(opt.getClass().getSimpleName());
                    }
                }
                break;
            case STRESS_TEST:
                for (var opt : options) {
                    if (opt.getClass() == StressOptions.class) {
                        run_internal(testClass, opt, resultPath);
                    } else {
                        printInvalidOption(opt.getClass().getSimpleName());
                    }
                }
                break;
            case MODEL_CHECKING:
                for (var opt : options) {
                    if (opt.getClass() == ModelCheckingOptions.class) {
                        run_internal(testClass, opt, resultPath);
                    } else {
                        printInvalidOption(opt.getClass().getSimpleName());
                    }
                }
                break;
        }
    }

    static private void printInvalidOption(String unknownOptionName) {
        System.err.printf("Invalid Option type: %s\n", unknownOptionName);
    }

    /**
     * Run a single LinCheck test on a given test class
     *
     * @param testClass the class to be tested with operation annotated methods
     * @param options   the options to be tested with
     */
    static public void run(@NotNull Class<?> testClass, Options<?, ?> options) {
        run_internal(testClass, options, null);
    }

    static private void run_internal(@NotNull Class<?> testClass, Options<?, ?> options, String path) {
        try {
            if (options.getClass() == ModelCheckingOptions.class || options.getClass() == StressOptions.class) {
                System.out.printf("Executing linearizability test in %s.\n", optionRepString(options));
                try {
                    LinChecker.check(testClass, options);
                } catch (LincheckAssertionError lincheckAssertionError) {
                    System.err.printf("Linearizability test in %s failed with the following execution trace:\n",
                            optionRepString(options));
                    System.err.println(lincheckAssertionError.getMessage());
                    throw lincheckAssertionError;
                }
                System.out.printf("Linearizability test in %s exited successfully.\n", optionRepString(options));
                System.out.println();
            }
        } catch (Exception | Error e) {
            log_result(false,  e.toString(), options, path);
            throw e;
        }
        log_result(true, "", options, path);
    }


    /**
     * string representation for classes of Options types
     *
     * @param options the option to get the string for
     * @return the corresponding representation string
     */
    static private String optionRepString(Options<?, ?> options) {
        if (options.getClass() == ModelCheckingOptions.class) {
            return "model checking mode";
        } else if (options.getClass() == StressOptions.class) {
            return "stress testing mode";
        } else {
            return "unknown mode";
        }
    }

}
