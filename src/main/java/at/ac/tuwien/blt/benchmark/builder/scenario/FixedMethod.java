package at.ac.tuwien.blt.benchmark.builder.scenario;

import java.lang.reflect.Method;

/**
 * A method to be used in a scenario with fixed parameters
 */
public class FixedMethod {
    private final Method method;
    private final Object[] params;

    public FixedMethod(Method method, Object[] params) {
        this.method = method;
        this.params = params;
    }

    @Override
    public String toString() {
        StringBuilder paramBuilder = new StringBuilder();
        for (var p : params) {
            paramBuilder.append(p.toString()).append(", ");
        }
        return method.getName() + "(" + paramBuilder.substring(0, paramBuilder.length() - 2) + ")";
    }

    public int getParamCount() {
        return params.length;
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getParams() {
        return params;
    }
}
