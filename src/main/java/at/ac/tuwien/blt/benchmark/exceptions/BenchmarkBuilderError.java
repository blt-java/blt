package at.ac.tuwien.blt.benchmark.exceptions;

/**
 * This error is thrown when an unrecoverable error occurs during benchmark building. This error is mostly thrown
 * due to internal problems in benchmark building (i.e. IOExceptions in file writing operations)
 */
public class BenchmarkBuilderError extends Error {
    public BenchmarkBuilderError(String message) {
        super(message);
    }
}
