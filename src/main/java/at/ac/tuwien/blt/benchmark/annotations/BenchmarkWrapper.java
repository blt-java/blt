package at.ac.tuwien.blt.benchmark.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a wrapperClass that is intended to be used to generate benchmarks. This annotation is required to
 * call a BenchmarkRunner on a wrapper class.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface BenchmarkWrapper {
    boolean debug() default false;
}
