package at.ac.tuwien.blt.benchmark.builder.compiler;

import javax.tools.SimpleJavaFileObject;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URI;

/**
 * Helper Object for java source code generation. Stores a compiled Java class.
 */
public class JavaByteObject extends SimpleJavaFileObject {
    private final ByteArrayOutputStream outputStream;

    /**
     * Constructs a new JavaByteObject.
     *
     * @param name the name of the compilation unit represented by this file object
     */
    public JavaByteObject(String name) {
        super(URI.create("bytes:///" + name.replaceAll("\\.", "/")),
                Kind.CLASS);
        outputStream = new ByteArrayOutputStream();
    }

    @Override
    public OutputStream openOutputStream() {
        return outputStream;
    }

    public byte[] getBytes() {
        return outputStream.toByteArray();
    }
}
