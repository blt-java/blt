package at.ac.tuwien.blt.benchmark.config.paramgen.distribution;

/**
 * Logistic distribution to inexpensively approximate normal distribution for parameter generation
 * <p>
 * Implementation taken from "William H. Press, Saul A. Teukolsky, William T. Vetterling,
 * "Brian P. Flannery - Numerical recipes", Cambridge University Press (2007), 3rd edition, page 322
 */
public class LogisticDistribution extends Distribution {
    final double mu;
    final double sigma;

    public LogisticDistribution(double mu, double sigma) {
        this.mu = mu;
        this.sigma = sigma;
    }

    @Override
    public double inverseCdf(double p) {
        if (p < 0 || p > 1) {
            throw new UnsupportedOperationException("p must be between 0 and 1");
        }
        return mu + 0.551328895421792049 * sigma * Math.log(p / (1. - p));
    }

    @Override
    public double cdf(double val) {
        double e = Math.exp(-Math.abs(1.81379936423421785 * (val - mu) / sigma));
        if (val >= mu) {
            return 1.0 / (1.0 + e);
        } else {
            return e / (1.0 + e);
        }
    }
}
