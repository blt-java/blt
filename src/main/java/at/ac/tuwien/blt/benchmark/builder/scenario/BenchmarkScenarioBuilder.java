package at.ac.tuwien.blt.benchmark.builder.scenario;

import at.ac.tuwien.blt.benchmark.config.options.BenchmarkConfiguration;

import java.util.ArrayList;
import java.util.Arrays;

public class BenchmarkScenarioBuilder implements BenchmarkScenario, ChainedBenchmarkScenarioBuilder {

    private final ArrayList<ThreadScenario> threadScenarios;
    private int scenarioSize;

    public BenchmarkScenarioBuilder() {
        this.threadScenarios = new ArrayList<>();
        this.scenarioSize = -1;
    }

    @Override
    public ChainedBenchmarkScenarioBuilder scenarioSize(int size) {
        this.scenarioSize = size;
        return this;
    }

    @Override
    public ChainedBenchmarkScenarioBuilder addThreadScenarios(ThreadScenario... threadScenarios) {
        this.threadScenarios.addAll(Arrays.asList(threadScenarios));
        return this;
    }

    @Override
    public String getScenarioString() {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    public int getThreads() {
        return threadScenarios.size();
    }

    @Override
    public int getScenarioSize() {
        return scenarioSize;
    }

    @Override
    public ChainedBenchmarkScenarioBuilder addThreadScenario(ThreadScenario threadScenario) {
        this.threadScenarios.add(threadScenario);
        return this;
    }

    @Override
    public BenchmarkScenario build() {
        return this;
    }

    @Override
    public FixedMethod[][] getFixedScenario(int threads, int scenarioSize) {
        FixedMethod[][] fixed = new FixedMethod[threads][scenarioSize];
        for (int i = 0; i < threads; i++) {
            for (int j = 0; j < scenarioSize; j++) {
                int s = threadScenarios.get(i % threadScenarios.size()).getScenarioMethods().size();
                fixed[i][j] = threadScenarios.get(i % threadScenarios.size()).getScenarioMethods().get(j % s).getFixed();
            }
        }
        return fixed;
    }

    @Override
    public FixedMethod[][] getFixedScenario() {
        int threads = threadScenarios.size();
        return getFixedScenarioInternal(threads, false);
    }

    @Override
    public FixedMethod[][] getFixedScenario(int threads) {
        return getFixedScenarioInternal(threads, true);
    }

    private FixedMethod[][] getFixedScenarioInternal(int threads, boolean displayWarning) {
        if (displayWarning) {
            if (threads < threadScenarios.size()) {
                System.err.println("WARNING: (BenchmarkScenarioBuilder) the specified scenario has more threads " +
                        "than the benchmark was defined for, ignoring redundant threads.");
            }
        }
        int ss;
        int maxThreadScenario = threadScenarios.stream()
                .mapToInt(s -> s.getScenarioMethods().size()).max().orElse(1);

        if (scenarioSize == -1) {
            ss = genScenarioSize(maxThreadScenario, BenchmarkConfiguration.DEFAULT_SCENARIO_SIZE);
        } else {
            ss = genScenarioSize(maxThreadScenario, scenarioSize);
        }
        return getFixedScenario(threads, ss);
    }

    /**
     * Gets the actual scenario size for a given thread scenario
     */
    private int genScenarioSize(int source, int target) {
        int size = source;
        while (size < target) {
            size += source;
        }
        return size;
    }
}
