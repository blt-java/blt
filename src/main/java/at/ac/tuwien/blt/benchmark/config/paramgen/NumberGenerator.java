package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

/**
 * A number parameter generator that generates numbers based on a specified probability
 * distribution and constrained range.
 *
 * @param <T> the number type that should be generated
 */
public interface NumberGenerator<T extends Number> extends ParameterGenerator<T> {
    /**
     * Get the probability distribution of this ParameterGenerator
     *
     * @return the probability distribution
     */
    Distribution getDistribution();

    /**
     * Get the max possible value of the number generator
     *
     * @return the maximum range constraint
     */
    T getMaxValue();

    /**
     * Get the min possible value of the number generator
     *
     * @return the minimum range constraint
     */
    T getMinValue();

    /**
     * Gets the generation type for the this ParameterGenerator
     *
     * @return the class the generator generates values of
     */
    Class<T> getGenerationType();
}
