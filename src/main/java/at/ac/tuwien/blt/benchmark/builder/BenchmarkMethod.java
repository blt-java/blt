package at.ac.tuwien.blt.benchmark.builder;

import at.ac.tuwien.blt.benchmark.config.paramgen.ParameterGenerators;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;

/**
 * Stores a method together with its parameter generators and internal id for benchmark building
 */
public class BenchmarkMethod {
    final private Method method;
    final private ParameterGenerator<?>[] methodParameterGenerators;
    final private int id;

    public Method getMethod() {
        return method;
    }

    public ParameterGenerator<?>[] getMethodParameterGenerators() {
        return methodParameterGenerators;
    }

    public BenchmarkMethod(int id, Method method, Map<String, ParameterGenerator<?>> parameterGeneratorMap) {
        this.method = method;
        this.methodParameterGenerators = new ParameterGenerator[method.getParameterCount()];
        this.id = id;

        int i = 0;
        for (Parameter parameter : method.getParameters()) {
            // insert custom parameter generator or else default
            if (parameterGeneratorMap != null && parameterGeneratorMap.containsKey(parameter.getName())) {
                methodParameterGenerators[i] = parameterGeneratorMap.get(parameter.getName());
            } else {
                methodParameterGenerators[i] = ParameterGenerators.defaultParamGen(parameter.getType());
            }
            i++;
        }
    }

    public int getId() {
        return id;
    }
}
