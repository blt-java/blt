package at.ac.tuwien.blt.benchmark.config.options;

import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenario;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenario;
import at.ac.tuwien.blt.benchmark.exceptions.BenchmarkConfigurationError;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.util.Optional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BenchmarkOptionsBuilder implements BenchmarkOptions, ChainedBenchmarkOptionsBuilder {

    protected final Map<String, ParameterGenerator<?>> paramGenerators = BenchmarkConfiguration.DEFAULT_PARAMETER_GENERATORS;
    protected int forks = BenchmarkConfiguration.DEFAULT_FORKS;
    protected TimeUnit timeUnit = BenchmarkConfiguration.DEFAULT_TIME_UNIT;
    protected String resultPath = BenchmarkConfiguration.DEFAULT_OUTPUT_PATH;
    protected String baseFolderName = BenchmarkConfiguration.DEFAULT_BASE_FOLDER_NAME;
    protected boolean maximizesEffectiveUpdates = BenchmarkConfiguration.DEFAULT_MAXIMIZE_EFFECTIVE_UPDATES;
    protected Options jmhOptions = BenchmarkConfiguration.DEFAULT_JMH_OPTIONS;
    protected int initialSize = BenchmarkConfiguration.DEFAULT_INITIAL_SIZE;
    protected int[] threadCount = BenchmarkConfiguration.DEFAULT_THREAD_COUNT;
    protected BenchmarkScenario benchmarkScenario;
    protected ThreadScenario initScenario;
    protected final Date date;
    protected Integer measurementIterations;

    public BenchmarkOptionsBuilder() {
        // set benchmark date on options creation
        date = new Date();
    }

    @Override
    public ChainedBenchmarkOptionsBuilder measurementIterations(int iterations) {
        this.measurementIterations = iterations;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder forks(int forks) {
        if (forks < 0) {
            throw new BenchmarkConfigurationError("Number of forks must be a positive integer");
        }
        this.forks = forks;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder threads(int... threadCounts) {
        if (threadCounts.length == 0) {
            throw new BenchmarkConfigurationError("the supplied threadCount array must not be empty");
        }
        this.threadCount = threadCounts;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder threads(int threadCount) {
        if (threadCount < 1) {
            throw new BenchmarkConfigurationError("Number of threads must be a positive integer");
        }
        this.threadCount = new int[] {threadCount};
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder timeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder resultPath(String path) {
        this.resultPath = path;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder initialSize(int size) {
        if (initScenario != null) {
            System.err.println("WARNING: (BenchmarkOptionsBuilder) initialSize is not compatible with initScenario.");
        }
        if (size < 0) {
            throw new BenchmarkConfigurationError("Initial size must be 0 or a positive integer");
        }
        this.initialSize = size;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder benchmarkScenario(BenchmarkScenario benchmarkScenario) {
        if (benchmarkScenario == null) {
            throw new BenchmarkConfigurationError("benchmark scenario must not be null!");
        }
        this.benchmarkScenario = benchmarkScenario;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder initScenario(ThreadScenario initScenario) {
        if (initialSize != BenchmarkConfiguration.DEFAULT_INITIAL_SIZE) {
            System.err.println("WARNING: (BenchmarkOptionsBuilder) initialSize is not compatible with initScenario.");
        }
        this.initScenario = initScenario;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder baseFolder(String fileName) {
        if (fileName.contains("\\") || fileName.contains("/")) {
            throw new BenchmarkConfigurationError("Base file name may not contain any front or backslashes");
        }
        this.baseFolderName = fileName;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder maximizeEffectiveUpdates(boolean maximizesEffectiveUpdates) {
        this.maximizesEffectiveUpdates = maximizesEffectiveUpdates;
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder addParamGenerator(String paramName, ParameterGenerator<?> paramGenerator) {
        this.paramGenerators.put(paramName, paramGenerator);
        return this;
    }

    @Override
    public ChainedBenchmarkOptionsBuilder jmhOptions(Options jmhOptions) {
        this.jmhOptions = jmhOptions;
        return this;
    }

    @Override
    public BenchmarkOptions build() {
        return this;
    }

    @Override
    public Optional<Integer> getForks() {
        return Optional.of(forks);
    }

    @Override
    public Optional<TimeUnit> getTimeUnit() {
        return Optional.of(timeUnit);
    }

    @Override
    public String getFolderName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        return String.format("%s/%s_%s/", resultPath, baseFolderName,
                sdf.format(date));
    }

    @Override
    public boolean maximizeEffectiveUpdates() {
        return maximizesEffectiveUpdates;
    }

    @Override
    public Map<String, ParameterGenerator<?>> getParamGenerators() {
        return paramGenerators;
    }

    @Override
    public Options getJmhOptions() {
        return jmhOptions;
    }

    @Override
    public String getResultFolder() {
        return resultPath;
    }

    @Override
    public int[] getThreads() {
        return threadCount;
    }

    @Override
    public int getInitialSize() {
        return initialSize;
    }

    @Override
    public BenchmarkScenario getBenchmarkScenario() {
        return benchmarkScenario;
    }

    @Override
    public ThreadScenario getInitScenario() {
        return initScenario;
    }

    @Override
    public Integer getMeasurementIterations() {
        return measurementIterations;
    }
}
