package at.ac.tuwien.blt.benchmark.builder.scenario;

import at.ac.tuwien.blt.benchmark.config.paramgen.ParameterGenerators;
import at.ac.tuwien.blt.benchmark.exceptions.BenchmarkScenarioError;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class ThreadScenarioBuilder implements ThreadScenario, ChainedThreadScenarioBuilder {
    // wrapper class method for contains check
    private final Class<?> wrapperClass;

    // parameters for benchmark data
    private final ArrayList<ScenarioMethod> methods;

    public ThreadScenarioBuilder(Class<?> wrapperClass) {
        this.wrapperClass = wrapperClass;
        this.methods = new ArrayList<>();
    }

    @Override
    public ArrayList<ScenarioMethod> getScenarioMethods() {
        return methods;
    }

    private void checkValidity(Method method) {
        if (Arrays.stream(wrapperClass.getMethods()).noneMatch(m -> m.equals(method))) {
            throw new BenchmarkScenarioError("Method \"" + method.getName() + "\" is not a member of" +
                    "the wrapper class \"" + wrapperClass.getName() + "\"");
        }
    }

    @Override
    public ChainedThreadScenarioBuilder addMethodCall(Method m) {
        checkValidity(m);

        ArrayList<ParameterGenerator<?>> paramGens = new ArrayList<>();
        for (var p : m.getParameters()) {
            ParameterGenerator<?> paramGen = ParameterGenerators.defaultParamGen(p.getType());
            if (paramGen == null) {
                throw new BenchmarkScenarioError("No default parameter generator available for param \"" +
                        p.getName() + "\" of method \"" + m.getName() + "\". " +
                        "Please provide a ParameterGenerator implementation!");
            }
            paramGens.add(paramGen);
        }
        ParameterGenerator<?>[] p = new ParameterGenerator[paramGens.size()];
        for (int i = 0; i < paramGens.size(); i++) {
            p[i] = paramGens.get(i);
        }

        this.methods.add(new ScenarioMethod(m, p));

        return this;
    }

    @Override
    public ChainedThreadScenarioBuilder addMethodCall(Method m, ParameterGenerator<?>... paramGens) {
        checkValidity(m);

        // check whether parameter generated types match
        int i = 0;
        for (var pg : paramGens) {
            var paramType = m.getParameters()[i].getType();
            if (!(paramType.isInstance(pg.generate()))) {
                throw new BenchmarkScenarioError("Invalid parameter generator type for param \"" +
                        m.getParameters()[i].getName() + "\" of method \"" + m.getName() + "\"");
            }
            i++;
        }

        this.methods.add(new ScenarioMethod(m, paramGens));

        return this;
    }

    @Override
    public ChainedThreadScenarioBuilder addMethodCall(Method m, Object... params) {
        checkValidity(m);

        int i = 0;
        for (var p : params) {
            var paramType = m.getParameters()[i].getType();
            if (!(paramType.isInstance(p))) {
                throw new BenchmarkScenarioError("Invalid parameter type for param \"" +
                        m.getParameters()[i].getName() + "\" of method \"" + m.getName() + "\"");
            }
            i++;
        }

        this.methods.add(new ScenarioMethod(m, params));

        return this;
    }

    private void checkCount(int count, Method m) {
        if (count < 0) {
            throw new BenchmarkScenarioError("Count for method \"" + m.getName() + "\" must be 0 or a positive integer.");
        } else if (count == 0) {
            System.err.println("WARNING: (BenchmarkScenarioBuilder) provided count for method \""
                    + m.getName() + "\" is 0 and has no effect");
        }
    }

    @Override
    public ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m) {
        checkCount(count, m);

        for (int i = 0; i < count; i++) {
            addMethodCall(m);
        }

        return this;
    }

    @Override
    public ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m, ParameterGenerator<?>... paramGens) {
        checkCount(count, m);

        for (int i = 0; i < count; i++) {
            addMethodCall(m, paramGens);
        }

        return this;
    }

    @Override
    public ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m, Object... params) {
        checkCount(count, m);

        for (int i = 0; i < count; i++) {
            addMethodCall(m, params);
        }

        return this;
    }

    @Override
    public FixedMethod[] getFixed() {
        FixedMethod[] fm = new FixedMethod[methods.size()];
        for (int i = 0; i < methods.size(); i++) {
            fm[i] = methods.get(i).getFixed();
        }
        return fm;
    }

    public ThreadScenario build() {
        return this;
    }
}
