package at.ac.tuwien.blt.benchmark.exceptions;

/**
 * This error is thrown when an unrecoverable error occurs during benchmark configuration
 * (mainly inside BenchmarkOptions)
 */
public class BenchmarkConfigurationError extends Error {
    public BenchmarkConfigurationError(String message) {
        super(message);
    }
}
