package at.ac.tuwien.blt.benchmark.config.options;

import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenario;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenario;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;
import org.openjdk.jmh.runner.options.Options;

import java.util.concurrent.TimeUnit;

public interface ChainedBenchmarkOptionsBuilder {

    /**
     * Sets the number of forks (i.e. "fresh" JVM instances) to be used in the benchmark
     *
     * @param value number of forks
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder forks(int value);

    /**
     * Sets the number of threads to be used in the benchmark. This will override the current number of threads of the
     * ChainedBenchmarkOptionBuilder if it has been supplied before.
     * <p>
     * If the size of the supplied array is larger than 1, all benchmarks will be executed
     * once for every supplied number of threads and saved in a separate output file.
     *
     * @param threadCounts array of number of threads to be used
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder threads(int... threadCounts);

    /**
     * Sets the number of threads to be used in the benchmark. This will override the current number of threads of the
     * ChainedBenchmarkOptionBuilder if it has been supplied before.
     *
     * @param threadCount number of threads to be used
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder threads(int threadCount);

    /**
     * Sets the time unit to be used in the benchmark. This will influence the units in the benchmark output file.
     * If, for example, time unit is set to TimeUnit.MICROSECONDS, results of a throughput test will be displayed as
     * "ops/us"
     *
     * @param timeUnit time unit to be used in the benchmark
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder timeUnit(TimeUnit timeUnit);

    /**
     * Sets the output path for benchmark output files. Depending on whether benchmarks are executed with different
     * numbers of threads, multiple output files may be produced.
     * <p>
     * Paths can either be relative (i.e. the path "" corresponds to the class path) or absolute.
     *
     * @param path base path for benchmark output files
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder resultPath(String path);

    /**
     * Sets the initial size of the data structure to be benchmarked. This corresponds to the number of elements the
     * data structure is filled with before the benchmarks are run.
     *
     * @param size initial size of the data structure.
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder initialSize(int size);

    /**
     * Sets the base file name for the benchmark output file folder. This name will be appended with the Unix Time stamp
     * to differentiate between different runs
     *
     * @param fileName base file name of the benchmark output files.
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder baseFolder(String fileName);

    /**
     * Used to set whether threads should try to update the data structure as often as possible by alternating between
     * inserting and removing the same value perpetually. This results in the data structure being effectively updated
     * as often as possible.
     *
     * @param maximizesEffectiveUpdates if true, threads will alternate between inserting and removing the same (unique) value
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder maximizeEffectiveUpdates(boolean maximizesEffectiveUpdates);

    /**
     * Used to add a parameter generator for a given parameter name. This can be used to constrain the value range of
     * parameters or to set a unbalance value for the used parameters of a method.
     *
     * @param paramName      the name of the parameter to be generated with the supplied generator
     * @param paramGenerator the parameter generator to be used
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder addParamGenerator(String paramName, ParameterGenerator<?> paramGenerator);

    /**
     * Add JMH options. Note that all options that are contained in the ChainedBenchmarkOptionsBuilder will be
     * overwritten. Therefore, only use JMH options to set parameters that cannot be controlled with
     * ChainedBenchmarkOptionsBuilder methods like, for example, the JVM path.
     * <p>
     * The following parameters should (and mostly can) *not* be controlled by the supplied jmhOptions:
     * - threads
     * - threadGroups
     * - forks
     * - measurement* (i.e. measurementIterations, measurementBatchSize, ...)
     * - output
     * - result
     * - timeUnits
     *
     * @param jmhOptions JMH options
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder jmhOptions(Options jmhOptions);

    /**
     * Sets the benchmark scenario for the benchmark configuration.
     *
     * @param benchmarkScenario the scenario to be set
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder benchmarkScenario(BenchmarkScenario benchmarkScenario);

    /**
     * Set the number of measurement iterations to be used in the JMH benchmark
     *
     * @param iterations the number of iterations in the JMH benchmark
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder measurementIterations(int iterations);

    /**
     * Sets the benchmark scenario for the benchmark initialization
     *
     * @param initScenario the scenario to be set
     * @return a ChainedBenchmarkOptionsBuilder with the set parameter
     */
    ChainedBenchmarkOptionsBuilder initScenario(ThreadScenario initScenario);

    /**
     * Produce the final BenchmarkOptions from a ChainedBenchmarkOptionsBuilder
     *
     * @return the corresponding BenchmarkOptions
     */
    BenchmarkOptions build();
}
