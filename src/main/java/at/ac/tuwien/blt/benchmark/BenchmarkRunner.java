package at.ac.tuwien.blt.benchmark;

import at.ac.tuwien.blt.benchmark.builder.BenchmarkBuilder;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptionsBuilder;
import at.ac.tuwien.blt.benchmark.eval.ResultEvaluator;
import at.ac.tuwien.blt.benchmark.exceptions.BenchmarkRunnerError;
import org.jetbrains.annotations.NotNull;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import sun.misc.Unsafe;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is the entry point for the benchmarking component of this framework. It executes benchmarks for a
 * specified wrapperClass for a data structure and allows to configure the benchmarks using a BenchmarkOption object
 */
public class BenchmarkRunner {
    /**
     * Generates and runs benchmarks for the specified class with default options
     *
     * @param testClass the wrapperClass to test
     * @throws BenchmarkRunnerError when an uncaught exception is thrown during the JMH benchmark
     */
    static public void run(@NotNull Class<?> testClass) {
        run(testClass, new BenchmarkOptionsBuilder().build());
    }

    /**
     * Generates and runs benchmarks for the specified class with specified options
     *
     * @param testClass        the wrapperClass to test
     * @param benchmarkOptions custom benchmark options for configuration
     * @throws BenchmarkRunnerError when an uncaught exception is thrown during the JMH benchmark
     */
    static public void run(@NotNull Class<?> testClass, BenchmarkOptions benchmarkOptions) {

        disableIllegalAccessLogger();

        createResultDirectories(benchmarkOptions);

        // delete processed folder for successive benchmarking
        deleteDir(new File("build/classes/java/main/META-INF"));

        BenchmarkBuilder builder = new BenchmarkBuilder(testClass, benchmarkOptions);
        builder.compileClass();

        List<RunResult> results;
        try {
            results = new ArrayList<>(new Runner(builder.getJmhOptions()).run());
        } catch (RunnerException e) {
            throw new BenchmarkRunnerError(e.getMessage());
        }

        int scenarioSize = builder.getFixedScenarioSize();

        ResultEvaluator.evaluateBenchmarkResult(results, benchmarkOptions,
                builder.getScenarioRepresentation(), scenarioSize, builder.getJmhOptions());

    }

    /**
     * creates the benchmark result directories.
     *
     * @param benchmarkOptions the BenchmarkOptions for the current benchmark
     */
    private static void createResultDirectories(BenchmarkOptions benchmarkOptions) {
        String resultName = benchmarkOptions.getResultFolder();

        var folderName = benchmarkOptions.getFolderName();
        var resultPath = Paths.get(resultName);

        // create result directory if not present
        if (Files.exists(resultPath)) {
            try {
                Files.createDirectories(resultPath);
            } catch (IOException e) {
                throw new RuntimeException("Could not create result data directory in \"" + resultPath.toString() + "\"");
            }
        }

        // create data directory
        var folderPath = Paths.get(folderName);
        try {
            Files.createDirectories(folderPath);
        } catch (IOException e) {
            throw new RuntimeException("Could not create result data directory in \"" + folderPath.toString() + "\"");
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            assert children != null;
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));

                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * Disables the java reflection illegal access logger, this is a "hack" to circumvent warning messages arising
     * from an illegal access operation in JMH V1.21
     */
    private static void disableIllegalAccessLogger() {
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe u = (Unsafe) theUnsafe.get(null);

            Class<?> cls = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field logger = cls.getDeclaredField("logger");

            u.putObjectVolatile(cls, u.staticFieldOffset(logger), null);
        } catch (Exception e) {
            // ignore
        }
    }

}
