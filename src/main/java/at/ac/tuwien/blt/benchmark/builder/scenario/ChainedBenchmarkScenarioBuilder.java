package at.ac.tuwien.blt.benchmark.builder.scenario;

public interface ChainedBenchmarkScenarioBuilder {

    /**
     * adds a thread scenario to this BenchmarkScenario
     *
     * @param threadScenario the threadScenario to be added
     * @return this ChainedBenchmarkScenarioBuilder
     */
    ChainedBenchmarkScenarioBuilder addThreadScenario(ThreadScenario threadScenario);

    /**
     * Adds multiple thread scenarios to this BenchmarkScenario
     *
     * @param threadScenario the threadScenario to be added
     * @return this ChainedBenchmarkScenarioBuilder
     */
    ChainedBenchmarkScenarioBuilder addThreadScenarios(ThreadScenario... threadScenario);

    /**
     * Sets the scenario size (i.e. number of method calls per thread) for this BenchmarkScenario
     *
     * @param size the intended benchmark size
     * @return this ChainedBenchmarkScenarioBuilder
     */
    ChainedBenchmarkScenarioBuilder scenarioSize(int size);

    BenchmarkScenario build();
}
