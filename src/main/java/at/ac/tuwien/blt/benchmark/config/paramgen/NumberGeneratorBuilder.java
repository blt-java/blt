package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Builder class for number parameter generators
 *
 * @param <T> the class (extending number) to get the NumberGenerator for
 */
public class NumberGeneratorBuilder<T extends Number> {

    private final Class<T> generationType;
    private T minValue;
    private T maxValue;
    private Distribution distribution;


    public NumberGeneratorBuilder<T> constrainRange(T minValue, T maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        return this;
    }

    public NumberGeneratorBuilder<T> distribution(Distribution distribution) {
        this.distribution = distribution;
        return this;
    }

    public NumberGeneratorBuilder(Class<T> generationType) {
        this.generationType = generationType;
    }

    public NumberGeneratorBuilder(NumberGenerator<T> generator) {
        this.generationType = generator.getGenerationType();
        this.minValue = generator.getMinValue();
        this.maxValue = generator.getMaxValue();
        this.distribution = generator.getDistribution();
    }

    @SuppressWarnings("unchecked")
    public ParameterGenerator<T> build(@NotNull Class<?> generatorClass) {

        if (!ParameterGenerator.class.isAssignableFrom(generatorClass)) {
            throw new UnsupportedOperationException("the supplied generator class must " +
                    "implement NumericParameterGenerator.");
        }

        ParameterGenerator<T> parameterGenerator;

        try {

            if (distribution == null && isUnconstrained()) {
                Constructor<?> c = generatorClass.getConstructor();
                parameterGenerator = (ParameterGenerator<T>) c.newInstance();
            } else if (distribution == null) {
                Constructor<?> c = generatorClass.getConstructor(generationType, generationType, Distribution.class);
                parameterGenerator = (ParameterGenerator<T>) c.newInstance(minValue, maxValue,
                        new UniformDistribution((double) minValue, (double) maxValue));
            } else if (isUnconstrained()) {
                Constructor<?> c = generatorClass.getConstructor(Distribution.class);
                parameterGenerator = (ParameterGenerator<T>) c.newInstance(distribution);
            } else {
                Constructor<?> c = generatorClass.getConstructor(generationType, generationType, Distribution.class);
                parameterGenerator = (ParameterGenerator<T>) c.newInstance(minValue, maxValue, distribution);
            }
            return parameterGenerator;

        } catch (NoSuchMethodException e) {
            throw new UnsupportedOperationException("could not find a matching constructor for the supplied" +
                    "generator class");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private boolean isUnconstrained() {
        return maxValue == null || minValue == null;
    }
}
