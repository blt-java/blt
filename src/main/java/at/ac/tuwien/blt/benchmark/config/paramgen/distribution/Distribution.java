package at.ac.tuwien.blt.benchmark.config.paramgen.distribution;

/**
 * A probability distribution used in number parameter generators to generate values according to a given distribution
 */
public abstract class Distribution {

    /**
     * Gets the inverse cumulative probability density function of this distribution
     *
     * @param p the value to get inverseCdf for, p must be between 0 and 1
     * @return the inverseCdf of p
     */
    abstract public double inverseCdf(double p);

    /**
     * Gets the cumulative probability density function of this distribution
     *
     * @param val the value to get the cdf for
     * @return the inverseCdf of p
     */
    abstract public double cdf(double val);

    /**
     * Gets a random value distributed according to this distribution
     *
     * @return a random value generated according to this distribution
     */
    public double random() {
        return inverseCdf(Math.random());
    }
}
