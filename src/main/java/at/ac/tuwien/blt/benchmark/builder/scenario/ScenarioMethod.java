package at.ac.tuwien.blt.benchmark.builder.scenario;

import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.lang.reflect.Method;

/**
 * A method to be used in a scenario with possibly fixed parameters
 */
class ScenarioMethod {
    private final boolean hasFixedParams;
    private final Method method;
    private ParameterGenerator<?>[] parameterGenerators;
    private Object[] params;

    public ScenarioMethod(Method method, ParameterGenerator<?>[] parameterGenerators) {
        this.method = method;
        this.parameterGenerators = parameterGenerators;
        this.hasFixedParams = false;
    }

    public ScenarioMethod(Method method, Object[] params) {
        this.method = method;
        this.params = params;
        this.hasFixedParams = true;
    }

    public FixedMethod getFixed() {
        if (hasFixedParams) {
            return new FixedMethod(method, params);
        } else {
            Object[] arr = new Object[parameterGenerators.length];
            for (int i = 0; i < parameterGenerators.length; i++) {
                arr[i] = parameterGenerators[i].generate();
            }
            return new FixedMethod(method, arr);
        }
    }
}
