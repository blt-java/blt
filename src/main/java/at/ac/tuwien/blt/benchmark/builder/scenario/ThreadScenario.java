package at.ac.tuwien.blt.benchmark.builder.scenario;

import java.util.ArrayList;

public interface ThreadScenario {
    ArrayList<ScenarioMethod> getScenarioMethods();

    FixedMethod[] getFixed();
}
