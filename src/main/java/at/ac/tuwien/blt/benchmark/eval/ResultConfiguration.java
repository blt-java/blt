package at.ac.tuwien.blt.benchmark.eval;

/**
 * Default options for benchmark result configuration.
 */
public class ResultConfiguration {
    public static final String PERF_COUNTER_FILE = "perf_counters.csv";
    public static final String BENCHMARK_RESULT_FILE = "bench_results.csv";
    public static final String SCENARIO_FILE = "scenario.txt";
    public static final String JMH_RESULT = "jmh_report.json";
}
