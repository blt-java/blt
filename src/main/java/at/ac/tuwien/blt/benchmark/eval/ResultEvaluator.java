package at.ac.tuwien.blt.benchmark.eval;

import at.ac.tuwien.blt.benchmark.builder.BuilderConfiguration;
import at.ac.tuwien.blt.benchmark.builder.jmh.CounterMap;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.options.Options;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class wraps the result evaluation for the benchmarking component. Benchmark results are read, statistics
 * calculated and both are written to the corresponding output files
 */
public class ResultEvaluator {

    private static final double CONF95 = 1.96;
    private static final int MIN_ERROR_ITERATIONS = 3;

    public static void evaluateBenchmarkResult(Collection<RunResult> results, BenchmarkOptions benchmarkOptions,
                                               String scenarioString, int scenarioSize, Options jmhOptions) {
        var folderName = benchmarkOptions.getFolderName();

        var totalMeasurementIterations = jmhOptions.getMeasurementIterations().get() *
                jmhOptions.getForkCount().get();

        // read performance counter results
        List<String> filenames = Arrays.stream(benchmarkOptions.getThreads())
                .mapToObj(t -> String.format(BuilderConfiguration.PERFORMANCE_COUNTER_FORMAT_STRING, t))
                .collect(Collectors.toList());
        List<CounterMap> counterMaps = new ArrayList<>();

        for (String filename : filenames) {
            try {
                FileInputStream fileIn = new FileInputStream(filename);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                counterMaps.add((CounterMap) in.readObject());
                in.close();
                fileIn.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException("Error reading performance counters from file \"" + filename + "\"");
            }
        }

        // build performance counter csv results
        StringBuilder pcCsvBuilder = new StringBuilder();

        // first line
        var forkMeasurementIterations = jmhOptions.getMeasurementIterations().get();
        var warmupIterations = jmhOptions.getWarmupIterations().get();

        if (totalMeasurementIterations < MIN_ERROR_ITERATIONS) {
            pcCsvBuilder.append("threads, name, average \n");
        } else {
            pcCsvBuilder.append("threads, name, average, conf95 (+/-)\n");
        }

        if (!counterMaps.isEmpty()) {
            int threadIndex = 0;
            for (var counterMap : counterMaps) {
                for (var entry : counterMap.entrySet()) {
                    String counterName = entry.getKey();

                    List<Long> values = entry.getValue();

                    // remove warmup iterations
                    for (int i = 0; i < values.size(); i++) {
                        if (i % forkMeasurementIterations < warmupIterations) {
                            //noinspection SuspiciousListRemoveInLoop
                            values.remove(i);
                        }
                    }

                    // calculate mean
                    double mean = ((double) values.stream().mapToLong(l -> l).sum()) / values.size();

                    // calculate standard deviation
                    double standardDeviation = Math.sqrt(values.stream().mapToDouble(l -> (mean - l) * (mean - l)).sum() / values.size());

                    // confidence interval
                    double confInt = CONF95 * standardDeviation / Math.sqrt(values.size() - 1);

                    if (totalMeasurementIterations < MIN_ERROR_ITERATIONS) {
                        pcCsvBuilder.append(
                                String.format("%d, %s, %d\n",
                                        benchmarkOptions.getThreads()[threadIndex],
                                        counterName,
                                        (long) mean
                                )
                        );
                    } else {
                        pcCsvBuilder.append(String.format("%d, %s, %d, %d\n",
                                benchmarkOptions.getThreads()[threadIndex],
                                counterName,
                                (long) mean,
                                (long) confInt));
                    }
                }
                threadIndex++;
            }
        }

        boolean perfCountersPresent = pcCsvBuilder.toString().lines().count() > 1;

        // write performance counter results to csv file
        if (perfCountersPresent) {
            writeStringToFile(pcCsvBuilder.toString(), ResultConfiguration.PERF_COUNTER_FILE, folderName);
        }

        StringBuilder resultCsvBuilder = new StringBuilder();

        // evaluate results
        for (var result : results) {
            List<Double> aggregatedScores = result.getAggregatedResult().getIterationResults().stream()
                    .map(r -> r.getPrimaryResult().getScore()*scenarioSize).collect(Collectors.toList());

            double mean = aggregatedScores.stream().mapToDouble(r -> r).sum()
                    / result.getAggregatedResult().getIterationResults().size();

            double standardDeviation = Math.sqrt(aggregatedScores.stream()
                    .mapToDouble(l -> (mean - l) * (mean - l)).sum() / result.getAggregatedResult()
                    .getIterationResults().size());

            double confInt = CONF95 * standardDeviation / Math.sqrt(result.getAggregatedResult()
                    .getIterationResults().size() - 1);

            if (resultCsvBuilder.toString().isEmpty()) {
                resultCsvBuilder.append(String.format("threads, mean score (%s)",
                        result.getAggregatedResult().getScoreUnit()));
                if (totalMeasurementIterations < MIN_ERROR_ITERATIONS) {
                    resultCsvBuilder.append("\n");
                } else {
                    resultCsvBuilder.append(", conf95 (+/-)\n");
                }
            }
            var aggregatedResult = result.getAggregatedResult();
            if (totalMeasurementIterations < MIN_ERROR_ITERATIONS) {
                resultCsvBuilder.append(
                        String.format("%d, %f\n",
                                aggregatedResult.getParams().getThreads(),
                                aggregatedResult.getPrimaryResult().getScore()*scenarioSize
                        )
                );
            } else {
                resultCsvBuilder.append(
                        String.format("%d, %f, %f\n",
                                aggregatedResult.getParams().getThreads(),
                                aggregatedResult.getPrimaryResult().getScore(),
                                confInt
                        )
                );
            }
        }

        // write op results to csv file
        writeStringToFile(resultCsvBuilder.toString(), ResultConfiguration.BENCHMARK_RESULT_FILE, folderName);

        writeStringToFile(scenarioString, ResultConfiguration.SCENARIO_FILE, folderName);

        // print performance counters and results to stdout
        System.out.println("\n========== BENCHMARK RESULTS ==========");
        System.out.println("\nThroughput Results:");
        System.out.println(resultCsvBuilder);
        if (perfCountersPresent) {
            System.out.println("Performance Counter Results:");
            System.out.println(pcCsvBuilder);
        }

        System.out.printf("Benchmark results were saved to \"%s\"%n", folderName);
        System.out.println();
    }

    public static void writeStringToFile(String content, String filename, String folderName) {
        try {
            FileWriter fw = new FileWriter(folderName + filename);
            fw.write(content);
            fw.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
