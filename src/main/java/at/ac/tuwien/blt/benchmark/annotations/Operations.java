package at.ac.tuwien.blt.benchmark.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

/**
 * Utility methods for operation annotations
 */
public class Operations {
    /**
     * Valid operation annotations
     */
    public final static Class<?>[] validAnnotations = new Class<?>[]{
            InsertionOperation.class,
            ReadOnlyOperation.class,
            RemovalOperation.class,
            ReplacementOperation.class
    };

    private final static String[] insertionOperationNames = new String[]{
            "insert",
            "add",
            "push",
            "put"
    };

    private final static String[] removalOperationNames = new String[]{
            "remove",
            "pop"
    };

    private final static String[] readOnlyOperationNames = new String[]{
            "contains",
            "get",
            "size"
    };

    private final static String[] replacementOperationNames = new String[]{
            "replace",
            "set"
    };

    /**
     * Checks whether a method has more than one valid annotation
     *
     * @param method the method to be checked
     * @return whether the method was annotated with more than one valid annotation
     */
    public static boolean isInvalidlyAnnotated(Method method) {
        return Arrays.stream(method.getAnnotations()).filter(annotation ->
                Arrays.stream(validAnnotations).anyMatch(a -> a.equals(annotation.annotationType()))
        ).count() > 1;
    }

    /**
     * Gets the first valid operation annotation for a given method
     *
     * @param method the method to be queried for annotations
     * @return the first valid annotation or null if none are present
     */
    public static Class<? extends Annotation> getOperationAnnotation(Method method) {
        Optional<Annotation> annotation = Arrays.stream(method.getAnnotations()).filter(validAnnotation ->
                Arrays.stream(validAnnotations).anyMatch(a -> a.equals(validAnnotation.annotationType()))
        ).findFirst();
        return annotation.<Class<? extends Annotation>>map(Annotation::annotationType).orElse(null);
    }

    public static Class<? extends Annotation> inferAnnotation(Method method) {
        String methodName = method.getName();
        if (Arrays.asList(insertionOperationNames).contains(methodName)) {
            return InsertionOperation.class;
        } else if (Arrays.asList(removalOperationNames).contains(methodName)) {
            return RemovalOperation.class;
        } else if (Arrays.asList(readOnlyOperationNames).contains(methodName)) {
            return ReadOnlyOperation.class;
        } else if (Arrays.asList(replacementOperationNames).contains(methodName)) {
            return ReplacementOperation.class;
        } else {
            return null;
        }
    }
}
