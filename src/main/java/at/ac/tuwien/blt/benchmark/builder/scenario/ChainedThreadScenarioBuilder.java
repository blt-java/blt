package at.ac.tuwien.blt.benchmark.builder.scenario;

import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.lang.reflect.Method;

public interface ChainedThreadScenarioBuilder {
    /**
     * Adds a method call to this ThreadScenario and tries to imply parameter generators
     *
     * @param m the method to be added
     * @return this ChainedThreadScenarioBuilder
     * @throws at.ac.tuwien.blt.benchmark.exceptions.BenchmarkScenarioError if parameter generator implication fails
     */
    ChainedThreadScenarioBuilder addMethodCall(Method m);

    /**
     * Adds a method call with specified parameter generators to this ThreadScenario
     *
     * @param m         the method to be added
     * @param paramGens the parameter generators for the parameters of the method to be added
     * @return this ChainedThreadScenarioBuilder
     */
    ChainedThreadScenarioBuilder addMethodCall(Method m, ParameterGenerator<?>... paramGens);

    /**
     * Adds a method call with specified parameters to this ThreadScenario
     *
     * @param m      the method to be added
     * @param params the parameters for the method to be added
     * @return this ChainedThreadScenarioBuilder
     */
    ChainedThreadScenarioBuilder addMethodCall(Method m, Object... params);

    /**
     * Adds multiple method calls of a single method to this ThreadScenario and tries to imply parameter generators
     *
     * @param count the number of times the method should be added
     * @param m     the method to be added count times to the scenario
     * @return this ChainedThreadScenarioBuilder
     * @throws at.ac.tuwien.blt.benchmark.exceptions.BenchmarkScenarioError if parameter generator implication fails
     */
    ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m);

    /**
     * Adds multiple method calls of a single method to this ThreadScenario
     *
     * @param count     the number of times the method should be added
     * @param m         the method to be added count times to the scenario
     * @param paramGens parameters generators for the method parameters
     * @return this ChainedThreadScenarioBuilder
     */
    ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m, ParameterGenerator<?>... paramGens);

    /**
     * Adds multiple method calls of a single method to this ThreadScenario
     *
     * @param count  the number of times the method should be added
     * @param m      the method to be added count times to the scenario
     * @param params fixed parameters for the method
     * @return this ChainedThreadScenarioBuilder
     */
    ChainedThreadScenarioBuilder addMultipleMethodCalls(int count, Method m, Object... params);

    /**
     * Returns a ThreadScenario for the given builder options
     *
     * @return a ThreadScenario for the specified options
     */
    ThreadScenario build();
}
