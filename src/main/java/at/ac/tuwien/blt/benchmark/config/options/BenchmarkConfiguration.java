package at.ac.tuwien.blt.benchmark.config.options;

import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Default values for the benchmark configuration
 */
public class BenchmarkConfiguration {
    public final static Map<String, ParameterGenerator<?>> DEFAULT_PARAMETER_GENERATORS = new HashMap<>();
    public final static int DEFAULT_FORKS = 1;
    public final static int DEFAULT_MEASUREMENT_ITERATIONS = 5;
    public final static int DEFAULT_WARMUP_ITERATIONS = 2;
    public final static TimeUnit DEFAULT_TIME_UNIT = TimeUnit.MICROSECONDS;
    public final static String DEFAULT_OUTPUT_PATH = "result";
    public final static String DEFAULT_BASE_FOLDER_NAME = "benchmark";
    public final static boolean DEFAULT_MAXIMIZE_EFFECTIVE_UPDATES = false;
    public final static Options DEFAULT_JMH_OPTIONS = new OptionsBuilder().build();
    public final static int DEFAULT_INITIAL_SIZE = 0;
    public final static int DEFAULT_SCENARIO_SIZE = 1;
    public final static int[] DEFAULT_THREAD_COUNT = {1};
}
