package at.ac.tuwien.blt.benchmark.config.paramgen;

import org.jetbrains.kotlinx.lincheck.paramgen.StringGen;

/**
 * Default implementation for a String Parameter Generator. Wraps StringGen from kotlinx.lincheck
 */
public class StringGenerator extends StringGen {
    /**
     * Creates a new String generator
     */
    public StringGenerator() {
        super("");
    }

    /**
     * Creates a new String generator with specified maxLength
     *
     * @param maxLength max String length
     */
    public StringGenerator(int maxLength) {
        super(String.format("%d", maxLength));
    }

    /**
     * Creates a new String generator with specified maxLength and alphabet
     *
     * @param maxLength the max length for the String
     * @param alphabet  the alphabet to be used in the string generation
     */
    public StringGenerator(int maxLength, String alphabet) {
        super(String.format("%d:%s", maxLength, alphabet));
    }

    public StringGenerator(String configString) {
        super(configString);
    }
}
