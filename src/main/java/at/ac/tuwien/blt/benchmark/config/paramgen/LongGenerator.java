package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;

/**
 * A simple NumberParameterGenerator for type Long
 */
public class LongGenerator implements NumberGenerator<Long> {

    private final static Long MIN_VALUE = Long.MIN_VALUE;
    private final static Long MAX_VALUE = Long.MAX_VALUE;

    private final DoubleGenerator generator;
    private final Long minValue;
    private final Long maxValue;

    public LongGenerator(Long minValue, Long maxValue, Distribution distribution) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.generator = new DoubleGenerator(distribution);
    }

    public LongGenerator() {
        this(Long.MIN_VALUE, Long.MAX_VALUE, new UniformDistribution(MIN_VALUE, MAX_VALUE));
    }

    public LongGenerator(Distribution distribution) {
        this(Long.MIN_VALUE, Long.MAX_VALUE, distribution);
    }

    private Long constrain(Long value) {
        if (value > maxValue) {
            value = maxValue;
        } else if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    @Override
    public Distribution getDistribution() {
        return generator.getDistribution();
    }

    @Override
    public Long getMinValue() {
        return minValue;
    }

    @Override
    public Long getMaxValue() {
        return maxValue;
    }

    @Override
    public Long generate() {
        return constrain((long) Math.rint(generator.generate()));
    }

    @Override
    public Class<Long> getGenerationType() {
        return Long.class;
    }
}
