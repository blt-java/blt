package at.ac.tuwien.blt.benchmark.config.options;

import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenario;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenario;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.util.Optional;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface BenchmarkOptions {

    /**
     * Gets the specified number of forks for this BenchmarkOptions
     *
     * @return the number of forks or the default value if none was specified
     */
    Optional<Integer> getForks();

    /**
     * Gets the specified TimeUnit of forks for this BenchmarkOptions
     *
     * @return the number of forks or the default value if none was specified
     */
    Optional<TimeUnit> getTimeUnit();

    /**
     * Gets the specified resultPath for this BenchmarkOptions (i.e. the folder name results should be written to)
     *
     * @return the folder name or the default value if none was specified
     */
    String getFolderName();

    /**
     * Gets the specified result folder for this BenchmarkOptions (i.e. the folder containing
     * the benchmark result folders)
     *
     * @return the folder name or the default value if none was specified
     */
    String getResultFolder();

    /**
     * Returns whether the benchmark tries to generate a scenario maximizing effective updates by continuously adding
     * and removing the same value
     *
     * @return the number of forks or the default value none was specified
     */
    boolean maximizeEffectiveUpdates();

    /**
     * Gets a HashMap of the specified ParameterGenerators for this BenchmarkOptions
     * (i.e. the folder name results should be written to)
     *
     * @return the folder name or the default value if none was specified
     */
    Map<String, ParameterGenerator<?>> getParamGenerators();

    /**
     * Gets the specified jmhOptions for this BenchmarkOptions
     *
     * @return the folder name or the default jmhOptions if none were specified
     */
    Options getJmhOptions();

    /**
     * Gets the number of threads the benchmark was defined for. This may also be multiple
     *
     * @return an array containing the benchmark threads
     */
    int[] getThreads();

    /**
     * Gets the number of measurement iterations to be used in the JMH benchmark
     *
     * @return the number of measurement iterations for the JMH benchmark or null if none were provided
     */
    Integer getMeasurementIterations();

    /**
     * Gets the intended initial size for the benchmark data structure
     *
     * @return the number specified as initial size.
     */
    int getInitialSize();

    /**
     * Gets the initialization scenario for the benchmark
     *
     * @return the initialization scenario
     */
    ThreadScenario getInitScenario();

    /**
     * Gets the benchmark scenario for the benchmark
     *
     * @return the benchmark scenario
     */
    BenchmarkScenario getBenchmarkScenario();
}
