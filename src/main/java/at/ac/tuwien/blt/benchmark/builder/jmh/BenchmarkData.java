package at.ac.tuwien.blt.benchmark.builder.jmh;

import java.io.Serializable;

/**
 * Data structure for thread data during JMH Benchmark
 */
public class BenchmarkData implements Serializable {
    public final Object[][] parameters;
    public final int size;

    public BenchmarkData(Object[][] parameters, int benchmarkSize) {
        this.parameters = parameters;
        this.size = benchmarkSize;
    }
}
