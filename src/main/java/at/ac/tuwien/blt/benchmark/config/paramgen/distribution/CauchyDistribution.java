package at.ac.tuwien.blt.benchmark.config.paramgen.distribution;

/**
 * Cauchy distribution to inexpensively approximate normal distribution for parameter generation
 * <p>
 * Implementation taken from "William H. Press, Saul A. Teukolsky, William T. Vetterling,
 * "Brian P. Flannery - Numerical recipes", Cambridge University Press (2007) 3rd Edition, page 325
 */
public class CauchyDistribution extends Distribution {
    final double mu;
    final double sigma;

    public CauchyDistribution(double mu, double sigma) {
        this.mu = mu;
        this.sigma = sigma;
    }

    @Override
    public double cdf(double val) {
        return 0.5 + 0.318309886183790671 * Math.atan2(val - mu, sigma);
    }

    @Override
    public double inverseCdf(double p) {
        return mu + sigma * Math.tan(3.14159265358979324 * (p - 0.5));
    }
}
