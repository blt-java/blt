package at.ac.tuwien.blt.benchmark.exceptions;

/**
 * This error is thrown by the BenchmarkRunner if an unrecoverable error occurs during benchmark execution
 */
public class BenchmarkRunnerError extends Error {
    public BenchmarkRunnerError(String message) {
        super(message);
    }
}
