package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;

/**
 * A simple NumberParameterGenerator for type Float
 */
public class FloatGenerator implements NumberGenerator<Float> {

    private final static Float MIN_VALUE = Float.NEGATIVE_INFINITY;
    private final static Float MAX_VALUE = Float.POSITIVE_INFINITY;

    private final DoubleGenerator generator;
    private final Float minValue;
    private final Float maxValue;

    public FloatGenerator(Float minValue, Float maxValue, Distribution distribution) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.generator = new DoubleGenerator(distribution);
    }

    public FloatGenerator(Distribution distribution) {
        this(MIN_VALUE, MAX_VALUE, distribution);
    }

    public FloatGenerator() {
        this(MIN_VALUE, MAX_VALUE, new UniformDistribution(MIN_VALUE, MAX_VALUE));
    }

    private Float constrain(Float value) {
        if (value > maxValue) {
            value = maxValue;
        } else if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    @Override
    public Float generate() {
        return constrain(generator.generate().floatValue());
    }

    @Override
    public Distribution getDistribution() {
        return generator.getDistribution();
    }

    @Override
    public Float getMinValue() {
        return minValue;
    }

    @Override
    public Float getMaxValue() {
        return maxValue;
    }

    @Override
    public Class<Float> getGenerationType() {
        return Float.class;
    }
}