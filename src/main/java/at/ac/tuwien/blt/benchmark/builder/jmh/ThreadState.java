package at.ac.tuwien.blt.benchmark.builder.jmh;

import at.ac.tuwien.blt.benchmark.builder.BuilderConfiguration;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.ThreadParams;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Thread state for JMH benchmarks
 */
@State(Scope.Thread)
public class ThreadState {

    // thread-local benchmark parameters and method ids
    public Object[][] parameters;
    public int benchmarkSize;

    // counter to indicate position in arrays:
    public int position;

    @Setup
    public void prepareThread(ThreadParams tp) {
        int ti = tp.getThreadIndex();
        position = 0;

        BenchmarkData data;
        String filename = String.format(BuilderConfiguration.THREAD_DATA_FORMAT_STRING, ti + 1);

        try {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            data = (BenchmarkData) in.readObject();
            in.close();
            fileIn.close();
            parameters = data.parameters;
            benchmarkSize = data.size;

            if (parameters.length < data.size) {
                throw new RuntimeException("thread parameters array is too small for thread " + ti);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error reading thread benchmark data from file \"" + filename + "\"");
        }
    }

    @TearDown
    public void teardownThread(ThreadParams tp) {
        int ti = tp.getThreadIndex();
    }

}
