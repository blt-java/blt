package at.ac.tuwien.blt.benchmark.config.paramgen.distribution;

/**
 * Static value "distribution" whose probability density function is 1 at the given static value and 0 otherwise
 */
public class StaticValue extends Distribution {
    final double staticVal;

    public StaticValue(double staticVal) {
        this.staticVal = staticVal;
    }

    @Override
    public double inverseCdf(double p) {
        if (p < 0 || p > 1) {
            throw new UnsupportedOperationException("p must be between 0 and 1");
        }
        return staticVal;
    }

    @Override
    public double cdf(double val) {
        if (val >= staticVal) {
            return 1;
        } else {
            return 0;
        }
    }
}
