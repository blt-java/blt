package at.ac.tuwien.blt.benchmark.config.paramgen.distribution;

/**
 * Uniform distribution between a given value min and max
 */
public class UniformDistribution extends Distribution {

    final private double minValue;
    final private double maxValue;

    public UniformDistribution(double minValue, double maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public double inverseCdf(double p) {
        if (p < 0 || p > 1) {
            throw new UnsupportedOperationException("p must be between 0 and 1");
        }
        return p * (maxValue - minValue) + minValue;
    }

    @Override
    public double cdf(double val) {
        if (val < minValue) {
            return 0;
        } else if (val > maxValue) {
            return 1;
        } else {
            return (val - minValue) / (maxValue - minValue);
        }
    }
}
