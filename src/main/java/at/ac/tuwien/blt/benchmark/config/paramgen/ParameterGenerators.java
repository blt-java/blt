package at.ac.tuwien.blt.benchmark.config.paramgen;

import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

/**
 * Utility methods related to parameter generators
 */
public class ParameterGenerators {

    /**
     * Gets default parameter generator for a specified class
     *
     * @param paramClass the class to get the parameter generator for
     * @return the default parameter generator
     */
    public static ParameterGenerator<?> defaultParamGen(Class<?> paramClass) {
        if (paramClass.equals(Integer.class)) {
            return new IntGenerator();
        } else if (paramClass.equals(Double.class)) {
            return new DoubleGenerator();
        } else if (paramClass.equals(Float.class)) {
            return new FloatGenerator();
        } else if (paramClass.equals(Long.class)) {
            return new LongGenerator();
        } else if (paramClass.equals(String.class)) {
            return new StringGenerator();
        } else {
            return null;
        }
    }
}
