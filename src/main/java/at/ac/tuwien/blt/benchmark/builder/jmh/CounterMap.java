package at.ac.tuwien.blt.benchmark.builder.jmh;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * HashMap for benchmark performance counters
 */
public class CounterMap extends HashMap<String, ArrayList<Long>> {
    public ArrayList<Long> put(String key, Long value) {
        var list = super.get(key);
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(value);
        return super.put(key, list);
    }
}
