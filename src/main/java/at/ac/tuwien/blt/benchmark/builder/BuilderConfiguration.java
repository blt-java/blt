package at.ac.tuwien.blt.benchmark.builder;

/**
 * Default configuration values for BenchmarkBuilder
 */
public class BuilderConfiguration {

    // path and file format for thread data
    public final static String BENCHMARK_DATA_PATH = "build/benchmarkData";
    public final static String THREAD_DATA_FORMAT_STRING = BENCHMARK_DATA_PATH + "/td%02d.ser";
    public final static String PERFORMANCE_COUNTER_FORMAT_STRING = BENCHMARK_DATA_PATH + "/pc%02d.ser";

    // build directory for jmh classes
    public final static String JMH_BUILD_DIR = "build/classes/java/main";

    // benchmark class name
    public final static String GEN_CLASS_STR = "%sBenchmark";

    // path for resources and benchmark template
    public final static String BENCHMARK_TEMPLATE_NAME = "BenchmarkGroupTemplate.java";

    // package name for "processed" classes
    public final static String PACKAGE_NAME = "at.ac.tuwien.blt.processed.";
}

