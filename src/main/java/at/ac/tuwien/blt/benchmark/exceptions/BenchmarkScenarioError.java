package at.ac.tuwien.blt.benchmark.exceptions;

/**
 * This error is thrown by a BenchmarkScenarioBuilder or ThreadScenarioBuilder when an unrecoverable error occurs
 * during scenario generation
 */
public class BenchmarkScenarioError extends Error {
    public BenchmarkScenarioError(String message) {
        super(message);
    }
}
