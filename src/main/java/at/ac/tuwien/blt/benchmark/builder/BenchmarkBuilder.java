package at.ac.tuwien.blt.benchmark.builder;

import at.ac.tuwien.blt.benchmark.builder.compiler.JavaStringObject;
import at.ac.tuwien.blt.benchmark.builder.jmh.BenchmarkData;
import at.ac.tuwien.blt.benchmark.builder.scenario.*;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkConfiguration;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptionsBuilder;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.StaticValue;
import at.ac.tuwien.blt.benchmark.annotations.*;
import at.ac.tuwien.blt.benchmark.config.paramgen.*;
import at.ac.tuwien.blt.benchmark.eval.ResultConfiguration;
import at.ac.tuwien.blt.benchmark.exceptions.BenchmarkBuilderError;
import org.openjdk.jmh.generators.*;

import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import static at.ac.tuwien.blt.benchmark.builder.BuilderConfiguration.*;

import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.profile.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.options.*;
import org.openjdk.jmh.util.Optional;

import javax.tools.*;
import java.nio.file.*;
import java.util.*;
import java.io.*;
import java.lang.reflect.*;
import java.lang.annotation.Annotation;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;

/**
 * Object used to build a benchmark and generate benchmark data for a given configuration and wrapperClass
 */
public class BenchmarkBuilder {

    // class fields
    private final Map<Class<?>, List<BenchmarkMethod>> methods;
    private final List<BenchmarkMethod> methodList;
    private final List<Class<?>> presentAnnotations;
    private final BenchmarkOptions benchmarkOptions;
    private final Options jmhOptions;
    private final Class<?> wrapperClass;
    private final Map<String, String> performanceCounters;
    private final boolean debug;
    private FixedMethod[][] fixedScenario;
    private FixedMethod[] initMethods;

    private int idCounter;
    private int maxParams;

    /**
     * creates a new BenchmarkBuilder for a given wrapperClass and benchmarkOptions
     *
     * @param wrapperClass     class the benchmark should be generated for
     * @param benchmarkOptions the benchmark options (i.e. configuration) for the benchmark
     */
    public BenchmarkBuilder(Class<?> wrapperClass, BenchmarkOptions benchmarkOptions) {
        // check whether debug information is possibly present
        checkParameterMetadata(wrapperClass);

        // initialize id counter to 0
        idCounter = 0;
        maxParams = 0;

        // initialize methods
        methods = new HashMap<>();
        for (var annotation : Operations.validAnnotations) {
            methods.put(annotation, new ArrayList<>());
        }
        presentAnnotations = new ArrayList<>();

        // initialize method list
        methodList = new ArrayList<>();

        // initialize performance counters
        performanceCounters = new HashMap<>();

        // assign benchmarkOptions and wrapperClass
        this.benchmarkOptions = benchmarkOptions;

        if (!wrapperClass.isAnnotationPresent(BenchmarkWrapper.class)) {
            throw new BenchmarkBuilderError("Data structure wrapper class \"" +
                    wrapperClass.getSimpleName() + "\" is not marked with the @BenchmarkWrapper annotation");
        }
        this.wrapperClass = wrapperClass;

        // handle debug
        debug = wrapperClass.getAnnotation(BenchmarkWrapper.class).debug();

        // parse operation-annotated methods
        parseMethods(wrapperClass);

        // generate jmh options from benchmarkOptions
        this.jmhOptions = generateJMHOptions();

        // assert threads are defined and else set to 1
        if (benchmarkOptions.getThreads() == null || benchmarkOptions.getThreads().length == 0) {
            System.err.println("WARNING: Thread array is empty, defaulting to 1 thread.");
            ((BenchmarkOptionsBuilder) benchmarkOptions).threads(1);
        }

    }

    // helper method to get a string representation of a FixedMethod array
    private String getFixedMethodListString(FixedMethod[] methods) {
        StringBuilder repBuilder = new StringBuilder();
        repBuilder.append("{ ");
        for (int j = 0; j < methods.length; j++) {
            repBuilder.append(methods[j].toString());
            if (j != methods.length - 1) {
                repBuilder.append(", ");
            }
        }
        repBuilder.append(" }");
        return repBuilder.toString();
    }

    // gets a string representation for init and benchmark data of this builder
    public String getScenarioRepresentation() {
        StringBuilder scenarioBuilder = new StringBuilder();
        scenarioBuilder.append("init: ");
        scenarioBuilder.append(getFixedMethodListString(initMethods)).append("\n\n");
        int i = 1;
        for (var t : fixedScenario) {
            scenarioBuilder.append("thread ").append(i).append(": ");
            scenarioBuilder.append(getFixedMethodListString(t)).append("\n");
            i++;
        }
        return scenarioBuilder.toString();
    }

    // check whether parameter metadata looks like debug info is present
    private void checkParameterMetadata(Class<?> wrapperClass) {
        boolean debugInfoPresent = false;
        for (Method m : wrapperClass.getMethods()) {
            int n = 0;
            for (Parameter p : m.getParameters()) {
                if (!p.getName().equals("arg" + n)) {
                    debugInfoPresent = true;
                }
                n++;
            }
        }
        if (!debugInfoPresent) {
            System.err.println("WARNING: The wrapper class \"" + wrapperClass.getSimpleName() + "\" looks like it was " +
                    " compiled without parameter metadata (this will probably break ParameterGenerators)." +
                    " Are the sources compiled with -parameters?");
            System.err.println();
            System.err.flush();
        }
    }

    // parse wrapperClass methods and generate BenchmarkMethods with corresponding ids for the benchmark
    private void parseMethods(Class<?> wrapperClass) {
        for (var method : wrapperClass.getMethods()) {
            if (method.isAnnotationPresent(Operation.class)) {
                parseOperationType(method);
            } else if (method.isAnnotationPresent(PerformanceCounter.class)) {
                parsePerformanceCounter(method);
            }
        }
    }

    // parses performance counters for a given method
    private void parsePerformanceCounter(Method method) {
        // check return type
        if (!method.getReturnType().equals(long.class) && method.getReturnType().equals(Long.class)) {
            throw new BenchmarkBuilderError("Performance counter method " +
                    method.getName() + "() needs to return type long");
        }

        // check empty parameters
        if (method.getParameterCount() != 0) {
            throw new BenchmarkBuilderError("Parameter count of performance counter method " +
                    method.getName() + " is not empty");
        }

        // get counter name
        String counterName = method.getAnnotation(PerformanceCounter.class).counterName();
        if (counterName.isEmpty()) {
            counterName = method.getName();
        }

        if (performanceCounters.containsKey(counterName)) {
            throw new BenchmarkBuilderError("Duplicate Performance counter name + \"" + counterName + "\"\n" +
                    "Hint: if no counter name is specified, the retrieval method name is taken per default");
        }

        performanceCounters.put(counterName, method.getName());
    }

    // parses the operation type for a given method
    private void parseOperationType(Method method) {
        // throw error if more than one Operation annotation is present
        if (Operations.isInvalidlyAnnotated(method)) {
            throw new BenchmarkBuilderError("Method " + method.getName() + " has more than one operation annotations");
        }

        Class<? extends Annotation> annotation = Operations.getOperationAnnotation(method);

        // try to infer operation type if no annotation is present, else throw
        if (annotation == null) {
            annotation = Operations.inferAnnotation(method);
            if (annotation == null) {
                throw new BenchmarkBuilderError("Cannot infer operation type for method " + method.getName() +
                        ", please provide a annotation");
            }
        }

        BenchmarkMethod bm = new BenchmarkMethod(idCounter, method, benchmarkOptions.getParamGenerators());

        // insert method and parameter generator
        methods.get(annotation).add(bm);
        methodList.add(bm);

        // increment id counter
        idCounter++;

        // set max params
        if (method.getParameterCount() > maxParams) {
            maxParams = method.getParameterCount();
        }

        if (!presentAnnotations.contains(annotation)) {
            presentAnnotations.add(annotation);
        }
    }

    // generates jmh options from the benchmarkOptions jmhOptions to use in the JMH benchmark
    private Options generateJMHOptions() {
        OptionsBuilder builder = new OptionsBuilder();
        if (benchmarkOptions.getJmhOptions() != null) {
            Options jmhOpt = benchmarkOptions.getJmhOptions();

            // Benchmark modes
            for (Mode mode : jmhOpt.getBenchModes()) {
                builder.mode(mode);
            }

            // Profilers
            for (ProfilerConfig pc : builder.getProfilers()) {
                builder.addProfiler(pc.getKlass(), pc.getOpts());
            }

            // map all relevant options
            mapOptional(builder, jmhOpt.shouldFailOnError(), "shouldFailOnError");
            mapOptional(builder, jmhOpt.getMeasurementBatchSize(), "measurementBatchSize");
            mapOptional(builder, jmhOpt.getJvm(), "jvm");
            mapOptional(builder, jmhOpt.getMeasurementIterations(), "measurementIterations");
            mapOptional(builder, jmhOpt.getOperationsPerInvocation(), "operationsPerInvocation");
            mapOptional(builder, jmhOpt.getMeasurementTime(), "measurementTime");
            mapOptional(builder, jmhOpt.getTimeout(), "timeout");
            mapOptional(builder, jmhOpt.verbosity(), "verbosity");
            mapOptional(builder, jmhOpt.shouldDoGC(), "shouldDoGC");
            mapOptional(builder, jmhOpt.shouldSyncIterations(), "syncIterations");
            mapOptional(builder, jmhOpt.getWarmupBatchSize(), "warmupBatchSize");
            mapOptional(builder, jmhOpt.getWarmupIterations(), "warmupIterations");
            mapOptional(builder, jmhOpt.getWarmupTime(), "warmupTime");
            mapOptional(builder, jmhOpt.getWarmupMode(), "warmupMode");
            mapOptional(builder, jmhOpt.getForkCount(), "forks");

            // map jvm options
            var jvmArgs = jmhOpt.getJvmArgs();
            if (jvmArgs.hasValue()) {
                for (var o : jvmArgs.get()) {
                    builder.jvmArgs(o);
                }
            }

            jvmArgs = jmhOpt.getJvmArgsPrepend();
            if (jvmArgs.hasValue()) {
                for (var o : jvmArgs.get()) {
                    builder.jvmArgsPrepend(o);
                }
            }

            jvmArgs = jmhOpt.getJvmArgsAppend();
            if (jvmArgs.hasValue()) {
                for (var o : jvmArgs.get()) {
                    builder.jvmArgsAppend(o);
                }
            }

        }

        // override

        if (benchmarkOptions.getForks().hasValue()) {
            builder.forks(benchmarkOptions.getForks().get());
        }

        if (benchmarkOptions.getTimeUnit().hasValue()) {
            builder.timeUnit(benchmarkOptions.getTimeUnit().get());
        }

        if (benchmarkOptions.getMeasurementIterations() != null) {
            builder.measurementIterations(benchmarkOptions.getMeasurementIterations());
        } else {
            builder.measurementIterations(BenchmarkConfiguration.DEFAULT_MEASUREMENT_ITERATIONS);
        }

        if (builder.getWarmupIterations().hasValue()) {
            builder.warmupIterations(BenchmarkConfiguration.DEFAULT_WARMUP_ITERATIONS);
        }

        // set warmup iterations
        builder.warmupIterations(1);

        builder.threads(1);

        // set output path
        builder.resultFormat(ResultFormatType.JSON);
        builder.result(benchmarkOptions.getFolderName() + ResultConfiguration.JMH_RESULT);

        // profiler
        builder.addProfiler(StackProfiler.class);

        return builder.build();
    }

    // helper method to map optional parameters of a JMHOptions object to a new one
    private void mapOptional(OptionsBuilder builder, Optional<?> optional, String parameterName) {
        try {
            Method setter = Arrays.stream(builder.getClass().getMethods())
                    .filter(m -> (m.getName().equals(parameterName) && m.getParameters().length == 1) )
                    .findFirst().orElseThrow();
            if (optional.hasValue()) {
                setter.invoke(builder, optional.get());
            }
        } catch (Exception e) {
            throw new Error("Could not find method " + parameterName + " for class " +
                    builder.getClass().getName());
        }
    }

    // generates a thread scenario for the given threadId
    private ThreadScenario generateThreadScenario(int threadId) {
        ThreadScenario ts;

        if (benchmarkOptions.maximizeEffectiveUpdates()) {
            // check whether suitable insertion and removal methods exist

            List<BenchmarkMethod> insertionMethods = methods.get(InsertionOperation.class);
            List<BenchmarkMethod> removalMethods = methods.get(RemovalOperation.class);

            BenchmarkMethod insertionMethod = null, removalMethod = null;
            for (var im : insertionMethods) {
                for (var rm : removalMethods) {
                    var rp = rm.getMethod().getParameters();
                    var ip = im.getMethod().getParameters();

                    boolean allMatch;
                    allMatch = rm.getMethod().getParameterCount() == im.getMethod().getParameterCount();

                    for (var r: rp) {
                        for (var i: ip) {
                            if (r.getType() != i.getType()) {
                                allMatch = false;
                            }
                        }
                    }

                    if (allMatch) {
                        insertionMethod = im;
                        removalMethod = rm;
                    }
                }
            }

            if (insertionMethod == null) {
                throw new BenchmarkBuilderError("Couldn't find suitable insertion and removal methods");
            }

            int paramCount = insertionMethod.getMethod().getParameterCount();

            ParameterGenerator<?>[] staticGens =
                    new ParameterGenerator[paramCount];

            var params = insertionMethod.getMethod().getParameters();
            for (int i = 0; i < params.length; i++) {
                var defaultGenerator = ParameterGenerators.defaultParamGen(params[i].getType());
                if (defaultGenerator == null) {
                    throw new UnsupportedOperationException("Effective Update maximisation is only supported" +
                            "for NumberParameterGenerators in this version");
                }
                var generator = new NumberGeneratorBuilder<>
                        ((NumberGenerator<?>) defaultGenerator)
                        .distribution(new StaticValue(threadId))
                        .build(defaultGenerator.getClass());
                staticGens[i] = generator;
            }

            ThreadScenarioBuilder tb = new ThreadScenarioBuilder(wrapperClass);

            tb.addMethodCall(insertionMethod.getMethod(), fixParams(staticGens));
            tb.addMethodCall(removalMethod.getMethod(), fixParams(staticGens));

            ts = tb.build();

        } else {
            // by default, use all methods

            ThreadScenarioBuilder tb = new ThreadScenarioBuilder(wrapperClass);

            for (var method : methodList) {
                tb.addMethodCall(method.getMethod(), method.getMethodParameterGenerators());
            }

            ts = tb.build();
        }

        return ts;
    }

    // fixes and returns parameters for an array of parameter generators
    private Object[] fixParams(ParameterGenerator<?>... paramGens) {
        Object[] params = new Object[paramGens.length];
        for (int i = 0; i < paramGens.length; i++) {
            params[i] = paramGens[i].generate();
        }
        return params;
    }

    // converts FixedMethod array to BenchmarkData List
    private List<BenchmarkData> convertFixedMethodList(FixedMethod[][] fixedMethods) {
        List<BenchmarkData> bd = new ArrayList<>();
        for (var fm : fixedMethods) {
            bd.add(convertFixedMethods(fm));
        }
        return bd;
    }

    // converts FixedMethod array to BenchmarkData
    private BenchmarkData convertFixedMethods(FixedMethod[] fixedMethods) {
        int maxParams = Arrays.stream(fixedMethods).mapToInt(FixedMethod::getParamCount).max().orElse(0);
        Object[][] params = new Object[fixedMethods.length][maxParams];
        for (int i = 0; i < fixedMethods.length; i++) {
            params[i] = fixedMethods[i].getParams();
        }
        return new BenchmarkData(params, fixedMethods.length);
    }

    // generates benchmark data to pass to the benchmark threads
    private List<BenchmarkData> generateBenchmarkData(int maxThreads) {
        BenchmarkScenario bs = benchmarkOptions.getBenchmarkScenario();
        if (bs == null || benchmarkOptions.maximizeEffectiveUpdates()) {
            // no benchmarkScenario has been specified or maximize is true, generate thread scenarios
            var bb = new BenchmarkScenarioBuilder();
            for (int i = 0; i < maxThreads; i++) {
                bb.addThreadScenario(generateThreadScenario(i));
            }
            if (benchmarkOptions.maximizeEffectiveUpdates()) {
                bb.scenarioSize(2);
            }
            bs = bb.build();
        }

        // save fixed scenario for string representation
        fixedScenario = bs.getFixedScenario(maxThreads);

        return convertFixedMethodList(fixedScenario);
    }

    // generates initialization data according to the specified benchmarkOptions
    private BenchmarkData generateInitData() {
        Random r = new Random();
        ThreadScenario initScenario = benchmarkOptions.getInitScenario();

        if (initScenario == null) {
            ThreadScenarioBuilder tb = new ThreadScenarioBuilder(wrapperClass);

            for (int i = 0; i < benchmarkOptions.getInitialSize(); i++) {
                List<BenchmarkMethod> ops = methods.get(InsertionOperation.class);
                BenchmarkMethod method = ops.get(r.nextInt(ops.size()));
                tb.addMethodCall(method.getMethod(), method.getMethodParameterGenerators());
            }

            initScenario = tb;
        }

        initMethods = initScenario.getFixed();

        return convertFixedMethods(initMethods);
    }

    /**
     * Generate and compile classes for the given wrapper class and benchmark options
     */
    public void compileClass() {
        try {
            compileGroupClassInternal();
        } catch (IOException e) {
            if (e.getMessage() == null || e.getMessage().isEmpty()) {
                System.err.println("The following IOException occurred when generating classes:");
                System.err.println(e.getMessage());
            }
            throw new BenchmarkBuilderError("Failed to generate classes, exiting.");
        }
    }

    private String methodCallString(FixedMethod method, int index, String paramName) {
        String methodTemplate = "dataStructure.%s(%s)";
        String paramFormat = "(%s) %s";
        StringBuilder paramsString = new StringBuilder();

        Parameter[] params = method.getMethod().getParameters();
        for (int k = 0; k < params.length; k++) {
            Parameter p = params[k];
            paramsString.append(String.format(paramFormat, p.getType().getName(), paramName + "[" + index + "][" + k + "]"));
            if (k != params.length-1) paramsString.append(",");
        }

        return String.format(methodTemplate, method.getMethod().getName(), paramsString.toString());
    }

    // internal method to handle class compilation
    private void compileGroupClassInternal() throws IOException {
        int[] threads = benchmarkOptions.getThreads();

        String benchmarkClassName = String.format(GEN_CLASS_STR, wrapperClass.getSimpleName());

        // generate benchmark data once for maximum thread number
        List<BenchmarkData> benchmarkData = generateBenchmarkData(Arrays.stream(threads).max().orElseThrow());

        // create directory for thread data and delete old threadData if present
        Path path = Paths.get(BENCHMARK_DATA_PATH);

        // delete directory if already present
        if (Files.exists(path)) {
            try {
                var dirFiles = Files.list(path).collect(Collectors.toList());
                for (var filePath : dirFiles) {
                    Files.delete(filePath);
                }
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
                throw new IOException("Error deleting files in \"" + BENCHMARK_DATA_PATH + "\"");
            }
        }

        // create directory
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new IOException("Could not create thread data directory in \"" + path + "\"");
        }

        // write init data to file
        String initFile = String.format(THREAD_DATA_FORMAT_STRING, 0);
        BenchmarkData initData = generateInitData();
        try {
            FileOutputStream fileOut = new FileOutputStream(initFile);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(initData);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Failed to write init data to file \"" + initData + "\"");
        }

        // write benchmark data to files for each thread
        for (int i = 0; i < benchmarkData.size(); i++) {
            String threadFileName = String.format(THREAD_DATA_FORMAT_STRING, i + 1);
            try {
                FileOutputStream fileOut = new FileOutputStream(threadFileName);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(benchmarkData.get(i));
                out.close();
                fileOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new IOException("Failed to write thread data to file \"" + threadFileName + "\"");
            }
        }

        // generate benchmark source and compile with JMH annotation processor

        // generate parameter generator access
        StringBuilder performanceCounterString = new StringBuilder();
        for (var entry : performanceCounters.entrySet()) {
            String appendString = "\n\t\tcounterValue = dataStructure.%s();" +
                    "\n\t\tcounterMap.put(\"%s\", counterValue);";
            performanceCounterString.append(String.format(appendString, entry.getValue(), entry.getKey()));
        }

        // generate init String
        StringBuilder initCalls = new StringBuilder();
        for (int i = 0; i < initMethods.length; i++) {
            initCalls.append(methodCallString(initMethods[i], i, "initArgs"));
            initCalls.append(";\n\t\t");
        }

        // generate method String
        ArrayList<String> methodStrings = new ArrayList<>();

        for (int threadCount : threads) {
            StringBuilder methodString = new StringBuilder();
            methodString.append("\n");

            for (int i = 0; i < threadCount; i++) {
                methodString.append("\t@Group(\"g\")\n\t@GroupThreads(1)\n\t@Benchmark\n\tpublic void thread");
                methodString.append(String.format("%02d", (i + 1)));
                methodString.append("(Blackhole b, ThreadState ts) {");

                for (int j = 0; j < fixedScenario[i].length; j++) {
                    FixedMethod currentMethod = fixedScenario[i][j];
                    String appendString = "\n\t\tb.consume(%s);";
                    String methodCallString = methodCallString(currentMethod, j, "ts.parameters");

                    methodString.append(String.format(appendString, methodCallString));
                }

                methodString.append("\n\t}\n\n");
            }
            methodStrings.add(methodString.toString());
        }

        ArrayList<String> fileStrings = new ArrayList<>();
        int i = 0;
        for (var methodString: methodStrings)  {
            InputStream is = getClass().getClassLoader().getResourceAsStream(BENCHMARK_TEMPLATE_NAME);
            assert is != null;
            String stateContent = new BufferedReader(
                    new InputStreamReader(is, StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"));

            // state file
            String fileString = String.format(
                    // format string (template file content)
                    stateContent,

                    // (1) Benchmark Class Name
                    benchmarkClassName + String.format("%02d", threads[i]),

                    // (2) Wrapper Class Name for field type
                    wrapperClass.getName(),

                    // (3) Wrapper Class Name for data structure instance
                    wrapperClass.getName(),

                    // (4) init scenario
                    initCalls.toString(),

                    // (5) performance counter output
                    performanceCounterString.toString(),

                    // (6) benchmark group methods
                    methodString
            );
            fileStrings.add(fileString);

            i++;
        }

        if (debug) {
            System.out.println("Generated the following Java source files:");
            // print generated files
            for (var sc: fileStrings) {
                System.out.println(sc);
                System.out.println("==============================================");
            }
        }

        // compiler and file manager setup
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        StandardJavaFileManager standardJavaFileManager = compiler.getStandardFileManager(diagnostics,
                null, null);

        // create and execute compiler task
        List<JavaStringObject> compilationUnits = new ArrayList<>();
        List<String> classNames = new ArrayList<>();
        i = 0;
        for (var fs : fileStrings) {
            String name = PACKAGE_NAME + benchmarkClassName + String.format("%02d", threads[i]);
            classNames.add(name);
            compilationUnits.add(new JavaStringObject(name, fs));
            i++;
        }

        var compilationTask = compiler.getTask(null, standardJavaFileManager, diagnostics,
                Arrays.asList("-d", JMH_BUILD_DIR), classNames, compilationUnits);

        AbstractProcessor benchmarkProcessor = new BenchmarkProcessor();
        compilationTask.setProcessors(Collections.singletonList(benchmarkProcessor));
        compilationTask.call();


        // possibly print compiler diagnostics
        diagnostics.getDiagnostics().forEach(System.err::println);
    }

    /**
     * Gets the actual scenario size, which is the smallest multiple of the largest thread scenario size >= scenarioSize
     */
    public int getFixedScenarioSize() {
        return Arrays.stream(fixedScenario).mapToInt(s -> s.length).max().orElse(0);
    }

    /**
     * Gets the generated JMH options from this builder
     *
     * @return the builder JMH options (generated from BenchmarkOptions JMHOptions)
     */
    public Options getJmhOptions() {
        return jmhOptions;
    }
}
