package at.ac.tuwien.blt.benchmark.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a method returning a value of type long to be used as performance counter metric
 * <p>
 * The counter name can be used to identify the counter in the benchmark evaluation. If none is given the
 * method name is used by default.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PerformanceCounter {
    String counterName() default "";
}
