package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;

/**
 * A simple NumberParameterGenerator for type Int
 */
public class IntGenerator implements NumberGenerator<Integer> {

    private final static Integer MIN_VALUE = Integer.MIN_VALUE;
    private final static Integer MAX_VALUE = Integer.MAX_VALUE;

    private final DoubleGenerator generator;
    private final Integer minValue;
    private final Integer maxValue;

    public IntGenerator(Integer minValue, Integer maxValue, Distribution distribution) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.generator = new DoubleGenerator(distribution);
    }

    public IntGenerator() {
        this(MIN_VALUE, MAX_VALUE, new UniformDistribution(MIN_VALUE, MAX_VALUE));
    }

    public IntGenerator(Distribution distribution) {
        this(MIN_VALUE, MAX_VALUE, distribution);
    }

    private Integer constrain(Integer value) {
        if (value > maxValue) {
            value = maxValue;
        } else if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    @Override
    public Integer generate() {
        return constrain((int) Math.rint(generator.generate()));
    }

    @Override
    public Distribution getDistribution() {
        return generator.getDistribution();
    }

    @Override
    public Integer getMinValue() {
        return minValue;
    }

    @Override
    public Integer getMaxValue() {
        return maxValue;
    }

    @Override
    public Class<Integer> getGenerationType() {
        return Integer.class;
    }
}
