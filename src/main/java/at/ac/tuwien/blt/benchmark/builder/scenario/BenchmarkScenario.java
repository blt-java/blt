package at.ac.tuwien.blt.benchmark.builder.scenario;

public interface BenchmarkScenario {

    /**
     * Gets a human-readable String representation for scenario reporting purposes
     *
     * @return a human-readable String representation of the scenario
     */
    String getScenarioString();

    /**
     * Gets the number of threads the scenario was defined for
     *
     * @return the number of threads in this scenario
     */
    int getThreads();

    /**
     * Gets the maximum number of operations the scenario contains
     *
     * @return the number of threads in this scenario
     */
    int getScenarioSize();

    /**
     * Generates a scenario with fixed method arguments
     *
     * @return fixed scenario
     */
    FixedMethod[][] getFixedScenario();

    /**
     * Generates a scenario with fixed method arguments for a specified size and thread number
     *
     * @return fixed scenario
     */
    FixedMethod[][] getFixedScenario(int scenarioSize, int threads);

    /**
     * Generates a scenario with fixed method arguments for a specified thread number
     *
     * @return fixed scenario
     */
    FixedMethod[][] getFixedScenario(int maxThreads);
}
