package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.Distribution;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;

/**
 * A simple number parameter generator for type Double
 */
public class DoubleGenerator implements NumberGenerator<Double> {

    private final static Double MIN_VALUE = Double.NEGATIVE_INFINITY;
    private final static Double MAX_VALUE = Double.POSITIVE_INFINITY;

    private final Double minValue;
    private final Double maxValue;
    private final Distribution distribution;

    @Override
    public Double generate() {
        return constrain(distribution.random());
    }

    private double constrain(double value) {
        if (value > maxValue) {
            value = maxValue;
        } else if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    public DoubleGenerator() {
        this(MIN_VALUE, MAX_VALUE, new UniformDistribution(MIN_VALUE, MAX_VALUE));
    }

    public DoubleGenerator(Distribution distribution) {
        this(MIN_VALUE, MAX_VALUE, distribution);
    }

    public DoubleGenerator(Double minValue, Double maxValue, Distribution distribution) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.distribution = distribution;
    }

    @Override
    public Double getMinValue() {
        return minValue;
    }

    @Override
    public Double getMaxValue() {
        return maxValue;
    }

    @Override
    public Distribution getDistribution() {
        return distribution;
    }

    @Override
    public Class<Double> getGenerationType() {
        return Double.class;
    }
}
