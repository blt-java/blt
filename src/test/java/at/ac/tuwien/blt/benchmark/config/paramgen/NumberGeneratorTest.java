package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.StaticValue;
import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.UniformDistribution;
import org.junit.Test;

public class NumberGeneratorTest {

    @Test
    public void uniformDoubleGenTest() {
        int uniformMin = -1;
        int uniformMax = 1;

        double lower = -1;
        double upper = 1;

        var gen = new NumberGeneratorBuilder<>(Double.class)
                .distribution(new UniformDistribution(uniformMin, uniformMax))
                .constrainRange(lower, upper)
                .build(DoubleGenerator.class);

        for (int i = 0; i < 1000; i++) {
            var val = gen.generate();
            assert (upper >= val);
            assert (lower <= val);
        }
    }

    @Test
    public void uniformFloatGenTest() {
        int uniformMin = -2;
        int uniformMax = 2;

        float lower = -1;
        float upper = 1;

        var gen = new NumberGeneratorBuilder<>(Float.class)
                .constrainRange(lower, upper)
                .distribution(new UniformDistribution(uniformMin, uniformMax))
                .build(FloatGenerator.class);

        for (int i = 0; i < 1000; i++) {
            var val = gen.generate();
            assert (upper >= val);
            assert (lower <= val);
        }
    }

    @Test
    public void uniformLongGenTest() {
        int uniformMin = -2;
        int uniformMax = 2;

        Long lower = -1L;
        Long upper = 1L;

        var gen = new NumberGeneratorBuilder<>(Long.class)
                .constrainRange(lower, upper)
                .distribution(new UniformDistribution(uniformMin, uniformMax))
                .build(LongGenerator.class);

        for (int i = 0; i < 1000; i++) {
            var val = gen.generate();
            assert (upper >= val);
            assert (lower <= val);
        }
    }

    @Test
    public void uniformIntGenTest() {
        int uniformMin = -2;
        int uniformMax = 2;

        int lower = -1;
        int upper = 1;

        var gen = new NumberGeneratorBuilder<>(Integer.class)
                .distribution(new UniformDistribution(uniformMin, uniformMax))
                .constrainRange(lower, upper)
                .build(IntGenerator.class);

        for (int i = 0; i < 1000; i++) {
            var val = gen.generate();
            assert (upper >= val);
            assert (lower <= val);
        }
    }

    @Test
    public void staticValueTest() {
        double staticVal = 3;

        var gen = new NumberGeneratorBuilder<>(Double.class)
                .distribution(new StaticValue(staticVal))
                .build(DoubleGenerator.class);

        for (int i = 0; i < 1000; i++) {
            assert gen.generate() == staticVal;
        }
    }
}
