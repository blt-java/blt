package at.ac.tuwien.blt.benchmark.config.paramgen;

import org.jetbrains.kotlinx.lincheck.paramgen.StringGen;
import org.junit.Test;

public class StringGeneratorTest {
    @Test
    public void combinedTest() {
        StringGen stringGen = new StringGenerator(2,"A");
        for (int i = 0; i < 1000; i++) {
            var str = stringGen.generate();
            assert str.length() < 2 && (str.contains("A") || str.length() == 0);
        }
    }

    @Test
    public void alphabetTest() {
        StringGen stringGen = new StringGenerator(15,"ABC");
        for (int i = 0; i < 1000; i++) {
            var str = stringGen.generate();
            for (int j = 0; j < str.length(); j++) {
                assert str.charAt(j) == 'A' || str.charAt(j) == 'B' || str.charAt(j) == 'C';
            }
        }
    }

    @Test
    public void lengthTest() {
        StringGen stringGen = new StringGenerator(30);
        for (int i = 0; i < 1000; i++) {
            var str = stringGen.generate();
            assert str.length() < 30;
        }
    }
}
