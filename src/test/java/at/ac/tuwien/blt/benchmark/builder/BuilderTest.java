package at.ac.tuwien.blt.benchmark.builder;

import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptionsBuilder;
import at.ac.tuwien.blt.wrapperClasses.builtins.ArrayListWrapper;
import at.ac.tuwien.blt.wrapperClasses.builtins.LinkedListWrapper;
import org.junit.Test;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

public class BuilderTest {
    @Test
    public void jmhOptionsTest() {
        Options jmhOpt = new OptionsBuilder().shouldDoGC(false).build();
        BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .timeUnit(TimeUnit.MILLISECONDS)
                .jmhOptions(jmhOpt)
                .threads(1)
                .build();
        BenchmarkBuilder br = new BenchmarkBuilder(LinkedListWrapper.class, opt);
        br.compileClass();
    }
}
