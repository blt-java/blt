package at.ac.tuwien.blt.benchmark.config.paramgen;

import at.ac.tuwien.blt.benchmark.config.paramgen.distribution.*;
import org.junit.Test;

public class DistributionTest {

    final static double ALLOWED_ERROR = 1e-10;

    @Test
    public void cauchyTest() {
        Distribution c = new CauchyDistribution(0, 1);

        // assert that inverse cdf of 0.5 is mean
        assert (c.inverseCdf(0.5) == 0);
        assert (c.cdf(0) == 0.5);

        testDistribution(c);
    }

    @Test
    public void logisticTest() {
        Distribution l = new LogisticDistribution(0, 1);

        // assert that inverse cdf of 0.5 is mean
        assert (l.inverseCdf(0.5) == 0);
        assert (l.cdf(0) == 0.5);

        testDistribution(l);
    }

    @Test
    public void staticValueTest() {
        double statVal = 3;
        Distribution l = new StaticValue(statVal);

        // assert that static value has probability 1
        assert (l.inverseCdf(1) == statVal);
        assert (l.inverseCdf(0.5) == statVal);
        assert (l.inverseCdf(0) == statVal);

        assert (l.cdf(statVal - 1) == 0);
        assert (l.cdf(statVal) == 1);
        assert (l.cdf(statVal + 1) == 1);
    }

    @Test
    public void uniformTest() {
        Distribution u = new UniformDistribution(-5, 5);

        // some representative value tests
        assert (u.inverseCdf(0.25) == -2.5);
        assert (u.cdf(-2.5) == 0.25);

        assert (u.inverseCdf(1) == 5);
        assert (u.cdf(5) == 1);

        assert (u.inverseCdf(0.5) == 0);
        assert (u.cdf(0) == 0.5);

        testDistribution(u);
    }

    private void testDistribution(Distribution distribution) {
        // assert that inverse cdf of cdf(d) == d for -1 <= d < 1
        for (double d = -1; d < 1; d += 0.1) {
            assert (approxEqual(distribution.inverseCdf(distribution.cdf(d)), d));
        }

        // assert that inverse cdf of cdf(d) == d for 10 random d values
        for (int i = 0; i < 100; i++) {
            double d = Math.random() * 10 - 5;
            assert (approxEqual(distribution.inverseCdf(distribution.cdf(d)), d));
        }

        // assert that cdf of inverse cdf(d) == d for 0 <= d < 1
        for (double d = 0; d < 1; d += 0.1) {
            assert (approxEqual(distribution.cdf(distribution.inverseCdf(d)), d));
        }

        // assert that inverse cdf of cdf(d) == d for 10 random d values
        for (int i = 0; i < 100; i++) {
            double d = Math.random();
            assert (approxEqual(distribution.cdf(distribution.inverseCdf(d)), d));
        }
    }

    private boolean approxEqual(double d1, double d2) {
        return Math.abs(d1 - d2) < ALLOWED_ERROR;
    }
}
