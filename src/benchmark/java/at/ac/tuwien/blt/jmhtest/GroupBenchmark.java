package at.ac.tuwien.blt.jmhtest;
/*
import at.ac.tuwien.linbench.waitFreeLists.DraconicList;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.infra.ThreadParams;

public class GroupBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        DraconicList<Integer> q = new DraconicList<>();

        @Setup
        public void init() {
            q.add(1);
            q.add(2);
            q.add(3);
            q.add(4);
        }
    }

    @State(Scope.Thread)
    public static class ThreadState {

        int[] var;

        @Setup
        public void setup(ThreadParams tp) {
            var = new int[300];
            for (int i = 0; i < var.length; i++) {
                var[i] = tp.getThreadIndex();
            }
        }
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public void contains1(BenchmarkState bs, Blackhole b, ThreadState ts) {
        b.consume(bs.q.add(ts.var[0]));
        b.consume(bs.q.remove(ts.var[99]));
        b.consume(bs.q.contains(ts.var[199]));
        b.consume(bs.q.contains(ts.var[299]));
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public void contains2(BenchmarkState bs, Blackhole b, ThreadState ts) {
        b.consume(bs.q.add(ts.var[0]));
        b.consume(bs.q.remove(ts.var[99]));
        b.consume(bs.q.contains(ts.var[199]));
        b.consume(bs.q.contains(ts.var[299]));
    }
/*
    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public void contains3(BenchmarkState bs, Blackhole b, ThreadState ts) {
        b.consume(bs.q.add(ts.var[0]));
        b.consume(bs.q.remove(ts.var[99]));
        b.consume(bs.q.contains(ts.var[199]));
        b.consume(bs.q.contains(ts.var[299]));
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public void contains4(BenchmarkState bs, Blackhole b, ThreadState ts) {
        b.consume(bs.q.add(ts.var[0]));
        b.consume(bs.q.remove(ts.var[99]));
        b.consume(bs.q.contains(ts.var[199]));
        b.consume(bs.q.contains(ts.var[299]));
    }

    /*
    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public boolean contains5(BenchmarkState bs) {
        return bs.q.contains(5);
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public boolean contains6(BenchmarkState bs) {
        return bs.q.contains(6);
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public boolean contains7(BenchmarkState bs) {
        return bs.q.contains(7);
    }

    @Group("g")
    @GroupThreads(1)
    @Benchmark
    public boolean contains8(BenchmarkState bs) {
        return bs.q.contains(8);
    }

}
*/