package at.ac.tuwien.blt.objtest;

import org.jetbrains.kotlinx.lincheck.paramgen.ParameterGenerator;

import java.util.Random;

public class TempGen implements ParameterGenerator<Temperature> {
    Random r;
    public TempGen() {
       r = new Random();
    }

    @Override
    public Temperature generate() {
        return new Temperature(r.nextInt());
    }
}
