package at.ac.tuwien.blt.objtest;

import java.io.Serializable;
import java.util.Objects;

public class Temperature implements Serializable {
    public int value;
    public String unit;

    public Temperature(int value) {
        this.value = value;
        this.unit = "CELSIUS";
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return value == that.value && Objects.equals(unit, that.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }
}
