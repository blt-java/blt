package at.ac.tuwien.blt.objtest;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.concurrent.ConcurrentLinkedQueue;

@StressCTest
@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class TempListWrapper extends VerifierState {
    private final ConcurrentLinkedQueue<Temperature> q = new ConcurrentLinkedQueue<>();

    @Operation
    public boolean add(Temperature val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Temperature val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Temperature val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    public Object extractState() {
        return q;
    }

}
