package at.ac.tuwien.blt.wrapperClasses.waitFreeLists;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import at.ac.tuwien.blt.benchmark.annotations.PerformanceCounter;
import at.ac.tuwien.blt.waitFreeLists.DraconicList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.annotations.StateRepresentation;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

@StressCTest
@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class DraconicWrapper extends VerifierState {
    private final DraconicList<Integer> q = new DraconicList<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @StateRepresentation
    public String repStr() {
        return q.toString();
    }

    @PerformanceCounter
    public long trav() {
        return q.getTrav();
    }

    @PerformanceCounter
    public long rtry() {
        return q.getRtry();
    }

    @PerformanceCounter
    public long adds() {
        return q.getAdds();
    }

    @PerformanceCounter
    public long fails() {
        return q.getFail();
    }

    @PerformanceCounter
    public long cons() {
        return q.getCons();
    }

    @Override
    @NotNull
    protected Object extractState() {
        return q.toString();
    }
}
