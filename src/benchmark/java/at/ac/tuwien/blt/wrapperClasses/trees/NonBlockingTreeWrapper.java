package at.ac.tuwien.blt.wrapperClasses.trees;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;
import trees.lockfree.NonBlockingTorontoBSTMap;

@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class)
@Param(name = "key", gen = IntGen.class)
public class NonBlockingTreeWrapper extends VerifierState {
    private final NonBlockingTorontoBSTMap<Integer, Integer> q = new NonBlockingTorontoBSTMap<>();

    @Operation
    public Integer put(Integer key, Integer val) {
        return q.put(key, val);
    }

    @Operation
    public Integer remove(Integer key) {
        return q.remove(key);
    }

    @Operation
    public boolean contains(Integer key) {
        return q.containsValue(key);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q.toString();
    }
}
