package at.ac.tuwien.blt.wrapperClasses.arrays;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.Vector;

@BenchmarkWrapper
public class VectorWrapper extends VerifierState {
    private final Vector<Integer> q = new Vector<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q;
    }
}
