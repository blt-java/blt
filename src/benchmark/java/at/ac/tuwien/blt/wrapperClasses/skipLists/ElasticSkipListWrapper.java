package at.ac.tuwien.blt.wrapperClasses.skipLists;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import linkedlists.transactional.ElasticLinkedListIntSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

@StressCTest
@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class ElasticSkipListWrapper extends VerifierState {
    private final ElasticLinkedListIntSet q = new ElasticLinkedListIntSet();

    @Operation
    public boolean add(Integer val) {
        return q.addInt(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.removeInt(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.containsInt(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q.toString();
    }
}
