package at.ac.tuwien.blt.wrapperClasses.skipLists;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.concurrent.ConcurrentSkipListMap;

@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class)
@Param(name = "key", gen = IntGen.class)
public class ConcurrentSkipListMapWrapper extends VerifierState {
    private final ConcurrentSkipListMap<Integer, Integer> q = new ConcurrentSkipListMap<>();

    @Operation
    public Integer put(Integer key, Integer val) {
        return q.put(key, val);
    }

    @Operation
    public Integer remove(Integer key) {
        return q.remove(key);
    }

    @Operation
    public boolean contains(Integer key) {
        return q.containsValue(key);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q.toString();
    }
}
