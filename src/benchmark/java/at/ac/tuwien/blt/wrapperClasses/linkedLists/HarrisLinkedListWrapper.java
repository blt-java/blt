package at.ac.tuwien.blt.wrapperClasses.linkedLists;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import linkedlists.transactional.CompositionalLinkedListSortedSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

@StressCTest
@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class HarrisLinkedListWrapper extends VerifierState {
    private final CompositionalLinkedListSortedSet<Integer> q = new CompositionalLinkedListSortedSet<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q.toString();
    }
}
