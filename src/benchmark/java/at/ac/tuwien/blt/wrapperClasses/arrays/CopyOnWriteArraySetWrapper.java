package at.ac.tuwien.blt.wrapperClasses.arrays;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.concurrent.CopyOnWriteArraySet;

@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class)
public class CopyOnWriteArraySetWrapper extends VerifierState {
    private final CopyOnWriteArraySet<Integer> q = new CopyOnWriteArraySet<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q;
    }
}
