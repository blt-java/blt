package at.ac.tuwien.blt.wrapperClasses.waitFreeLists;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import at.ac.tuwien.blt.waitFreeLists.AbstractList;
import at.ac.tuwien.blt.waitFreeLists.SinglyLinkedCursorList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

@StressCTest
@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class SinglyLinkedCursorWrapper extends VerifierState {
    private final AbstractList<Integer> q = new SinglyLinkedCursorList<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q.toString();
    }
}
