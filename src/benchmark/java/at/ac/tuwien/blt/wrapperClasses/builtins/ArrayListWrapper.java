package at.ac.tuwien.blt.wrapperClasses.builtins;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.ArrayList;

@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class)
public class ArrayListWrapper extends VerifierState {
    private final ArrayList<Integer> q = new ArrayList<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @NotNull
    @Override
    protected Object extractState() {
        return q;
    }
}
