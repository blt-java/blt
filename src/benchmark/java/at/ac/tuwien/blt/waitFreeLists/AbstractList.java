package at.ac.tuwien.blt.waitFreeLists;

import java.util.concurrent.atomic.LongAdder;

public abstract class AbstractList<T> {
    /**
     * First list node
     */
    protected Node<T> head, tail;
    protected ThreadLocalCursor<T> cursor;
    protected LongAdder adds, rems, cons, trav, fail, rtry;

    /**
     * Constructor
     */
    public AbstractList() {
        this.head = new Node<>(Integer.MIN_VALUE);
        this.tail = new Node<>(Integer.MAX_VALUE);
        this.cursor = new ThreadLocalCursor<>(head);

        adds = new LongAdder();
        rems = new LongAdder();
        cons = new LongAdder();
        trav = new LongAdder();
        fail = new LongAdder();
        rtry = new LongAdder();

        while (!head.next.compareAndSet(null, tail, false, false)) ;
        while (!tail.prev.compareAndSet(null, head, false, false)) ;
    }

    /**
     * If element is present, returns node and predecessor. If absent, returns
     * node with least larger key.
     *
     * @param key key to search for
     *            node with least larger key.
     */
    public abstract void find(int key);

    /**
     * Add an element.
     *
     * @param item element to add
     * @return true iff element was not there already
     */
    public abstract boolean add(T item);

    /**
     * Test whether element is present
     *
     * @param item element to test
     * @return true iff element is present
     */
    public abstract boolean contains(T item);

    /**
     * Remove an element.
     *
     * @param item element to remove
     * @return true iff element was present
     */
    public abstract boolean remove(T item);

    /**
     * Generate String representation for current list
     * Needed for state extraction in lincheck
     *
     * @return string representation of list
     */
    @Override
    public String toString() {
        StringBuilder retstr = new StringBuilder("{");
        Node<T> curr = this.head;
        while (curr.next.getReference() != null) {
            curr = curr.next.getReference();
            if (curr.next.getReference() != null)
                retstr.append(curr.key).append(", ");
        }
        int l = retstr.length();
        if (l > 3) {
            retstr.delete(l - 2, l);
        }
        retstr.append("}");
        return retstr.toString();
    }

}
