package at.ac.tuwien.blt.waitFreeLists;

/**
 * Pair of adjacent list entries.
 */
class Window<T> {
    /**
     * Earlier node.
     */
    public Node<T> pred;
    /**
     * Later node.
     */
    public Node<T> curr;

    /**
     * Constructor.
     */
    Window(Node<T> pred, Node<T> curr) {
        this.pred = pred;
        this.curr = curr;
    }

}