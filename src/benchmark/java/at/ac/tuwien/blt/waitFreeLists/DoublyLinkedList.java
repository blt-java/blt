package at.ac.tuwien.blt.waitFreeLists;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class DoublyLinkedList<T> extends AbstractList<T> {

    @Override
    public void find(int key) {
        Node<T> pred, curr, succ;
        AtomicMarkableReference<Node<T>> next;
        boolean[] marked = {false}; // is curr marked?
        boolean snip;

        pred = cursor.get().pred;
        if (key <= pred.key) {
            pred = head;
        }

        retry:
        while (true) {
            while (pred.next.isMarked() || key <= pred.key) {
                pred = pred.prev.getReference();
            }
            curr = pred.next.getReference();

            do {
                succ = curr.next.get(marked);
                while (marked[0]) {           // replace curr if marked
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) {
                        next = pred.next;
                        if (next.isMarked()) {
                            continue retry;
                        }
                        succ = next.getReference();
                    } else {
                        succ.prev.set(pred, false);
                    }
                    curr = succ;
                    succ = succ.next.get(marked);
                }

                if (curr.prev.getReference() != pred) {
                    curr.prev.set(pred, false);
                }

                if (key <= curr.key) {
                    this.cursor.set(new Window<>(pred, curr));
                    return;
                }
                pred = curr;
                curr = curr.next.getReference();
            } while (true);
        }
    }

    @Override
    public boolean add(T item) {
        int key = item.hashCode();

        cursor.get().pred = head;
        while (true) {
            // find predecessor and current entries
            find(key);
            Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
            // is the key present?
            if (curr.key == key) {
                return false;
            } else {
                // splice in new node
                Node<T> node = new Node<>(item);
                node.next.set(curr, false);
                node.prev.set(pred, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    curr.prev.set(node, false);
                    return true;
                }
            }
        }
    }

    @Override
    public boolean contains(T item) {
        Node<T> curr;
        int key = item.hashCode();
        curr = head;
        while (key < curr.key) {
            curr = curr.prev.getReference();
        }

        while (key > curr.key) {
            curr = curr.next.getReference();
        }

        return (curr.key == key && !curr.next.isMarked());
    }

    @Override
    public boolean remove(T item) {
        int key = item.hashCode();
        boolean snip;

        // find predecessor and current entries
        find(key);
        Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
        // is the key present?
        if (curr.key != key) {
            return false;
        } else {
            // snip out matching node
            AtomicMarkableReference<Node<T>> succ = curr.next;
            do {
                if (succ.isMarked()) {
                    return false;
                }
                if (curr.next.compareAndSet(succ.getReference(), succ.getReference(), false, true)) {
                    break;
                }
            } while (true);

            pred.next.compareAndSet(curr, succ.getReference(), false, false);
            succ.getReference().prev.set(pred, false);
            return true;
        }
    }
}
