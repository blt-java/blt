package at.ac.tuwien.blt.waitFreeLists;

import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * list node
 */
class Node<T> {
    /**
     * actual item
     */
    T item;
    /**
     * item's hash code
     */
    int key;
    /**
     * next node in list
     */
    AtomicMarkableReference<Node<T>> next, prev;

    /**
     * Constructor for usual node
     *
     * @param item element in list
     */
    Node(T item) {      // usual constructor
        this.item = item;
        this.key = item.hashCode();
        this.next = new AtomicMarkableReference<>(null, false);
        this.prev = new AtomicMarkableReference<>(null, false);
    }

    /**
     * Constructor for sentinel node
     *
     * @param key should be min or max int value
     */
    Node(int key) { // sentinel constructor
        this.item = null;
        this.key = key;
        this.next = new AtomicMarkableReference<>(null, false);
        this.prev = new AtomicMarkableReference<>(null, false);
    }
}
