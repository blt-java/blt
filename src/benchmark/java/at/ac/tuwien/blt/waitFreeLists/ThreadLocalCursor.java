package at.ac.tuwien.blt.waitFreeLists;

public class ThreadLocalCursor<T> extends ThreadLocal<Window<T>> {
    private final Node<T> head;

    public ThreadLocalCursor(Node<T> head) {
        super();
        this.head = head;
    }

    @Override
    protected Window<T> initialValue() {
        return new Window<>(head, head.next.getReference());
    }


}
