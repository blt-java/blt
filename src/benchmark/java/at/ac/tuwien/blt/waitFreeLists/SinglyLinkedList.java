package at.ac.tuwien.blt.waitFreeLists;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class SinglyLinkedList<T> extends DraconicList<T> {

    @Override
    public boolean contains(T item) {
        Node<T> curr = head;
        while (curr.key < item.hashCode()) {
            curr = curr.next.getReference();
        }

        return (curr.key == item.hashCode() && !curr.next.isMarked());
    }

    @Override
    public void find(int key) {
        Node<T> pred, curr, succ;
        AtomicMarkableReference<Node<T>> next;
        boolean[] marked = {false}; // is curr marked?
        boolean snip;
        retry:
        while (true) {
            pred = cursor.get().pred;
            if (pred.next.isMarked() || key <= pred.key) {
                pred = head;
            }
            curr = pred.next.getReference();

            while (true) {
                succ = curr.next.get(marked);
                while (marked[0]) {           // replace curr if marked
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) {
                        next = pred.next;
                        if (next.isMarked()) {
                            continue retry;
                        }
                        succ = next.getReference();
                    }
                    curr = succ;
                    succ = succ.next.get(marked);
                }
                if (key <= curr.key) {
                    this.cursor.set(new Window<>(pred, curr));
                    return;
                }
                pred = curr;
                curr = curr.next.getReference();
            }
        }
    }

    @Override
    public boolean remove(T item) {
        int key = item.hashCode();
        boolean snip;

        // find predecessor and current entries
        find(key);
        Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
        // is the key present?
        if (curr.key != key) {
            return false;
        } else {
            // snip out matching node
            AtomicMarkableReference<Node<T>> succ = curr.next;
            do {
                if (succ.isMarked()) {
                    return false;
                }
                if (curr.next.compareAndSet(succ.getReference(), succ.getReference(), false, true)) {
                    break;
                }
            } while (true);

            pred.next.compareAndSet(curr, succ.getReference(), false, false);
            return true;
        }
    }
}
