package at.ac.tuwien.blt.waitFreeLists;

import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * Lock-free List based on M. Michael's algorithm.
 *
 * @author Maurice Herlihy
 */
public class DraconicList<T> extends AbstractList<T> {

    public boolean add(T item) {
        int key = item.hashCode();

        cursor.get().pred = head;
        while (true) {
            // find predecessor and current entries
            find(key);
            Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
            // is the key present?
            if (curr.key == key) {
                return false;
            } else {
                // splice in new node
                Node<T> node = new Node<>(item);
                node.next = new AtomicMarkableReference<>(curr, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    adds.increment();
                    return true;
                }
            }
            fail.increment();
        }
    }

    public boolean remove(T item) {
        int key = item.hashCode();
        boolean snip;
        while (true) {
            // find predecessor and current entries
            find(key);
            Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
            // is the key present?
            if (curr.key != key) {
                return false;
            } else {
                // snip out matching node
                Node<T> succ = curr.next.getReference();
                snip = curr.next.compareAndSet(succ, succ, false, true);
                if (!snip) {
                    fail.increment();
                    continue;
                }
                pred.next.compareAndSet(curr, succ, false, false);
                rems.increment();
                return true;
            }
        }
    }


    public boolean contains(T item) {
        int key = item.hashCode();
        // find predecessor and current entries
        find(key);
        Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
        cons.increment();
        return (curr.key == key);
    }

    public void find(int key) {
        Node<T> pred, curr, succ;
        boolean[] marked = {false}; // is curr marked?
        boolean snip;
        retry:
        while (true) {
            pred = head;
            curr = pred.next.getReference();
            trav.increment();
            while (true) {
                succ = curr.next.get(marked);
                while (marked[0]) {           // replace curr if marked
                    snip = pred.next.compareAndSet(curr, succ, false, false);
                    if (!snip) {
                        fail.increment();
                        rtry.increment();
                        continue retry;
                    }
                    curr = succ;
                    succ = curr.next.get(marked);
                    trav.increment();
                }
                if (curr.key >= key) {
                    this.cursor.set(new Window<>(pred, curr));
                    return;
                }
                pred = curr;
                curr = succ;
                trav.increment();
            }
        }
    }

    public long getTrav() {
        return trav.longValue();
    }

    public long getCons() {
        return cons.longValue();
    }

    public long getAdds() {
        return adds.longValue();
    }

    public long getFail() {
        return fail.longValue();
    }

    public long getRtry() {
        return rtry.longValue();
    }

}
