package at.ac.tuwien.blt.waitFreeLists;

public class SinglyLinkedCursorList<T> extends SinglyLinkedList<T> {

    public boolean add(T item) {
        int key = item.hashCode();

        while (true) {
            // find predecessor and current entries
            find(key);
            Node<T> pred = cursor.get().pred, curr = cursor.get().curr;
            // is the key present?
            if (curr.key == key) {
                return false;
            } else {
                // splice in new node
                Node<T> node = new Node<>(item);
                node.next.set(curr, false);
                if (pred.next.compareAndSet(curr, node, false, false)) {
                    return true;
                }
            }
        }
    }

    @Override
    public boolean contains(T item) {
        int key = item.hashCode();
        Node<T> curr = cursor.get().pred;
        if (key < curr.key) {
            curr = head;
        }

        while (key > curr.key) {
            curr = curr.next.getReference();
        }

        this.cursor.get().pred = curr;
        return (curr.key == key && !curr.next.isMarked());
    }
}
