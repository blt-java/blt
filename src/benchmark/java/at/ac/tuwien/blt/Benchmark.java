package at.ac.tuwien.blt;

import at.ac.tuwien.blt.benchmark.BenchmarkRunner;
import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenario;
import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenarioBuilder;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenario;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenarioBuilder;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptionsBuilder;
//import at.ac.tuwien.blt.jmhtest.ListBenchmark;
import at.ac.tuwien.blt.lincheck.LinCheckRunner;
import at.ac.tuwien.blt.objtest.TempGen;
import at.ac.tuwien.blt.objtest.TempListWrapper;
import at.ac.tuwien.blt.wrapperClasses.arrays.CopyOnWriteArraySetWrapper;
import at.ac.tuwien.blt.wrapperClasses.arrays.ReusableVectorWrapper;
import at.ac.tuwien.blt.wrapperClasses.hashtables.*;
import at.ac.tuwien.blt.wrapperClasses.linkedLists.*;
import at.ac.tuwien.blt.wrapperClasses.queue.ConcurrentLinkedQueueWrapper;
import at.ac.tuwien.blt.wrapperClasses.builtins.LinkedListWrapper;
import at.ac.tuwien.blt.wrapperClasses.arrays.VectorWrapper;
import at.ac.tuwien.blt.wrapperClasses.queue.ReusableLinkedQueueWrapper;
import at.ac.tuwien.blt.wrapperClasses.skipLists.ConcurrentSkipListMapWrapper;
import at.ac.tuwien.blt.wrapperClasses.skipLists.ElasticSkipListWrapper;
import at.ac.tuwien.blt.wrapperClasses.skipLists.NoHotSpotSkipListWrapper;
import at.ac.tuwien.blt.wrapperClasses.trees.FriendlyTreeWrapper;
import at.ac.tuwien.blt.wrapperClasses.trees.LogicalOrderingTreeWrapper;
import at.ac.tuwien.blt.wrapperClasses.trees.StandfordTreeWrapper;
import at.ac.tuwien.blt.wrapperClasses.trees.TransactionalRBTreeWrapper;
import at.ac.tuwien.blt.wrapperClasses.waitFreeLists.DraconicWrapper;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressOptions;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import trees.lockbased.LockBasedFriendlyTreeMap;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Benchmark {
    private static final int BENCHMARKS_PER_SET = 1;

    public static void main(String[] args) {
        //objTest();
        ArgumentParser parser = ArgumentParsers.newFor("LinBenchmark").build()
                .defaultHelp(true)
                .description("Benchmark and linearizability-checking framework for concurrent data structures");
        parser.addArgument("-b", "--benchmark")
                .type(Integer.class)
                .metavar("BENCHMARK_NUM")
                .help("Which benchmark to use");
        parser.addArgument("-l", "--lincheck")
                .type(Boolean.class)
                .metavar("RUN_LINCHECK")
                .setDefault(false)
                .help("Whether to run lincheck in addition to benchmarks (default is false)");
        parser.addArgument("-s", "--scenario")
                .type(Boolean.class)
                .metavar("SCENARIO_NUM")
                .setDefault(false)
                .help("Which benchmark scenario to run");
        try {
            Namespace ns = parser.parseArgs(args);
            int benchmarkSet = ns.get("benchmark");
            boolean runLincheck = ns.get("lincheck");
            synchroBenchmark(benchmarkSet, runLincheck);
            System.exit(0);
        } catch ( ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        /*
        try {
            scenarioBenchmark();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
         */
        //jmhTest();
        //BenchmarkOptions opt = new BenchmarkOptionsBuilder().measurementIterations(1).maximizeEffectiveUpdates(true).threads(1,4,8).build();
        //BenchmarkRunner.run(DraconicWrapper.class, opt);
        //LinCheckRunner.run(DraconicWrapper.class);
    }
    private static void objTest() {
        BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .measurementIterations(3)
                .addParamGenerator("val", new TempGen())
                .threads(1,2)
                .build();
        BenchmarkRunner.run(TempListWrapper.class, opt);
    }

    private static void synchroBenchmark(int benchmarkSet, boolean runLincheck) {
        SynchroBenchmark[] benchmarks = new SynchroBenchmark[] {
                new SynchroBenchmark(StandfordTreeWrapper.class, 1),
                new SynchroBenchmark(FriendlyTreeWrapper.class, 2),
                new SynchroBenchmark(LogicalOrderingTreeWrapper.class, 3),
                new SynchroBenchmark(TransactionalRBTreeWrapper.class, 7),
                new SynchroBenchmark(CopyOnWriteArraySetWrapper.class, 9),
                new SynchroBenchmark(VectorWrapper.class, 10),
                new SynchroBenchmark(ReusableVectorWrapper.class, 11),
                new SynchroBenchmark(ConcurrentHashMapWrapper.class, 12),
                new SynchroBenchmark(CliffHashMapWrapper.class, 13),
                new SynchroBenchmark(FriendlyHashMapWrapper.class, 14),
                new SynchroBenchmark(ResizableHashTableWrapper.class, 16),
                new SynchroBenchmark(ElasticHashTableWrapper.class, 17),
                new SynchroBenchmark(LazyLinkedListWrapper.class, 18),
                new SynchroBenchmark(LockCouplingListWrapper.class, 19),
                new SynchroBenchmark(SynchronizedSetWrapper.class, 20),
                new SynchroBenchmark(HarrisLinkedListWrapper.class, 21),
                new SynchroBenchmark(ReusableLinkedListWrapper.class, 22),
                new SynchroBenchmark(ElasticLinkedListWrapper.class, 23),
                new SynchroBenchmark(ConcurrentLinkedQueueWrapper.class, 24),
                new SynchroBenchmark(ReusableLinkedQueueWrapper.class, 25),
                new SynchroBenchmark(ConcurrentSkipListMapWrapper.class, 28),
                new SynchroBenchmark(NoHotSpotSkipListWrapper.class, 29),
                new SynchroBenchmark(ElasticSkipListWrapper.class, 31),
        };

        for (int i = 0; i < BENCHMARKS_PER_SET; i++) {
            var benchmark = benchmarks[ i + (benchmarkSet-1)*BENCHMARKS_PER_SET];

            BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .threads(1, 2, 4, 8, 16, 32)
                .baseFolder(benchmark.wrapper.getSimpleName())
                .build();
            BenchmarkRunner.run(benchmark.wrapper, opt);

            if (runLincheck)  {
                var resultPath = opt.getFolderName();
                try {
                    LinCheckRunner.run_stress_test(benchmark.wrapper, resultPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    LinCheckRunner.run_model_checking(benchmark.wrapper, resultPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void linkedListBenchmark() {
        simpleBenchmark(LinkedListWrapper.class);
    }

    private static void draconicBenchmark() {
        simpleBenchmark(DraconicWrapper.class);
    }

    private static void draconicLincheck() {
        var opt = new StressOptions();
        LinCheckRunner.run(DraconicWrapper.class, opt);
    }

    private static void simpleBenchmark(Class<?> clazz) {
        Options jmhOpt = new OptionsBuilder()
                .measurementIterations(1)
                .warmupIterations(1)
                .shouldDoGC(false)
                .build();
        BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .jmhOptions(jmhOpt)
                .forks(1)
                .maximizeEffectiveUpdates(false)
                .threads(1, 2)
                //.threads(4)
                .initialSize(10)
                .build();
        BenchmarkRunner.run(clazz, opt);
    }

    private static void scenarioBenchmark() throws NoSuchMethodException {
        // 1 thread only inserts, 2 threads check contain and 1 only removes

        Class<?> wrapperClass = DraconicWrapper.class;
        Method addMethod = wrapperClass.getMethod("add", Integer.class);
        Method removeMethod = wrapperClass.getMethod("remove", Integer.class);
        Method contains = wrapperClass.getMethod("contains", Integer.class);

        ThreadScenario con1 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 1).build();
        ThreadScenario con2 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 2).build();
        ThreadScenario con3 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 3).build();
        ThreadScenario con4 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 4).build();
        ThreadScenario con5 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 5).build();
        ThreadScenario con6 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 6).build();
        ThreadScenario con7 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 7).build();
        ThreadScenario con8 = new ThreadScenarioBuilder(wrapperClass).addMethodCall(contains, 8).build();

        ThreadScenario initSce = new ThreadScenarioBuilder(wrapperClass)
                .addMethodCall(addMethod, 1)
                .addMethodCall(addMethod, 2)
                .addMethodCall(addMethod, 3)
                .addMethodCall(addMethod, 4)
                .build();

        BenchmarkScenario bs = new BenchmarkScenarioBuilder()
                .addThreadScenarios(con1, con2, con3, con4, con5, con6, con7, con8)
                .scenarioSize(1)
                .build();

        BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .forks(1)
                //.threads(new int[]{1, 2, 3, 4, 5})
                .maximizeEffectiveUpdates(false)
                .benchmarkScenario(bs)
                .measurementIterations(1)
                .threads(1, 2, 4)
                .initScenario(initSce)
                .build();
        BenchmarkRunner.run(wrapperClass, opt);
    }

    static class SynchroBenchmark {
        public Class<?> wrapper;
        public int id;

        public SynchroBenchmark(Class<?> benchmark, int id) {
            this.wrapper = benchmark;
            this.id = id;
        }
    }

    /*
    private static void jmhTest() {
        Options opt1 = new OptionsBuilder()
                .include(ListBenchmark.class.getSimpleName())
                .threads(1)
                .warmupIterations(1)
                .measurementIterations(1)
                .forks(1)
                .timeUnit(TimeUnit.MICROSECONDS)
                .build();
        Options opt2 = new OptionsBuilder()
                .include(ListBenchmark.class.getSimpleName())
                .threads(2)
                .warmupIterations(1)
                .measurementIterations(1)
                .forks(1)
                .timeUnit(TimeUnit.MICROSECONDS)
                .build();
        Options opt4 = new OptionsBuilder()
                .include(ListBenchmark.class.getSimpleName())
                .threads(4)
                .forks(1)
                .warmupIterations(1)
                .measurementIterations(1)
                .timeUnit(TimeUnit.MICROSECONDS)
                .build();
        Options opt8 = new OptionsBuilder()
                .include(ListBenchmark.class.getSimpleName())
                .threads(8)
                .warmupIterations(1)
                .measurementIterations(1)
                .forks(1)
                .timeUnit(TimeUnit.MICROSECONDS)
                .build();

        try {
            new Runner(opt1).run();
            new Runner(opt2).run();
            new Runner(opt4).run();
            new Runner(opt8).run();
        } catch (RunnerException e) {
            e.printStackTrace();
        }
    }
     */

    private static void testString() {
        LockBasedFriendlyTreeMap<Integer, Integer> map = new LockBasedFriendlyTreeMap<>();
        System.out.println(map.toString());
        map.putIfAbsent(23,43);
        map.putIfAbsent(33,43);
        map.putIfAbsent(423,33);
        map.putIfAbsent(-423,33);
        System.out.println(map.toString());
    }
}

